#include "FullHdDPIScalingRule.h"

float UFullHdDPIScalingRule::GetDPIScaleBasedOnSize(FIntPoint Size) const
{
	FVector2D TargetResolution(1920, 1080);
	auto SidesDpi = FVector2D(Size) / TargetResolution;
	auto Dpi = FMath::Max(FMath::Min(SidesDpi.X, SidesDpi.Y), 0.1);
	// Small diff
	if (Dpi > 0.98 && Dpi < 1.02)
		Dpi = 1;
	return Dpi;
}