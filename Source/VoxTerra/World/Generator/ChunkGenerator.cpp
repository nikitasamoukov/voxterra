﻿#include "ChunkGenerator.h"

#include "LandGenerator.h"
#include "PerlinNoise/PerlinNoise.hpp"
#include "VoxTerra/Common/ArrayDim3.h"
#include "VoxTerra/Common/VoxLoad/VoxLoad.h"
#include "VoxTerra/World/Terrain/BlockTypeInfo.h"

FChunkGenerator::FChunkGenerator(int32 Seed)
{
	LandGenerator = MakeShared<FLandGenerator>(Seed);
}

TSharedPtr<FVoxChunk> FChunkGenerator::GenChunkAdvanced(FIntVector const& ChunkPos)
{
	PROFILE_FUNCTION()

	auto Chunk = MakeShared<FVoxChunk>();

	FIntVector PosChunkBase = ChunkPos * FVoxChunk::ChunkSize;

	FLandRectData LandData;
	LandData.PosBase = FIntVector2(PosChunkBase.X - 1, PosChunkBase.Y - 1);
	LandData.Pixels.SetSize({ FVoxChunk::ChunkSize + 2, FVoxChunk::ChunkSize + 2 });

	bool bIsLandReady = LandGenerator->TryGetLandRect(LandData);

	if (!bIsLandReady)
		return nullptr;

	struct FLandPixel
	{
		int16 HeightMin = 0;
		int16 HeightMax = 0;
	};

	TArray<TArrayDim2<FLandPixel>> LandMips;
	LandMips.SetNum(FVoxChunk::TreeLevels);
	for (int Level = 0; Level < FVoxChunk::TreeLevels; Level++)
	{
		auto Size = FVoxChunk::ChunkSize / FVoxChunk::LevelCubeSize(Level);
		LandMips[Level].SetSize({ Size, Size });
	}

	{
		TArray<FIntVector2> NeighOffsets = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

		int Level = FVoxChunk::TreeLevels - 1;
		auto& Mip = LandMips[Level];

		for (int x = 0; x < FVoxChunk::ChunkSize; x++)
			for (int y = 0; y < FVoxChunk::ChunkSize; y++)
			{
				FIntVector2 PosInLandTex(x + 1, y + 1);

				auto& Pixel = Mip.At(x, y);

				Pixel.HeightMax = LandData.Pixels.At(PosInLandTex).Height;
				Pixel.HeightMin = LandData.Pixels.At(PosInLandTex).Height;

				for (auto& Offset : NeighOffsets)
				{
					FIntVector2 PosNeigh(PosInLandTex.X + Offset.X, PosInLandTex.Y + Offset.Y);
					Pixel.HeightMax = FMath::Max(Pixel.HeightMax, LandData.Pixels.At(PosNeigh).Height);
					Pixel.HeightMin = FMath::Min(Pixel.HeightMin, LandData.Pixels.At(PosNeigh).Height);
				}
			}
	}

	for (int Level = FVoxChunk::TreeLevels - 2; Level >= 0; Level--)
	{
		auto& Mip = LandMips[Level];
		auto& MipNext = LandMips[Level + 1];

		for (int x = 0; x < Mip.Num().X; x++)
			for (int y = 0; y < Mip.Num().Y; y++)
			{
				auto& Pixel = Mip.At(x, y);

				auto& Pixel1 = MipNext.At(x * 2 + 0, y * 2 + 0);
				auto& Pixel2 = MipNext.At(x * 2 + 0, y * 2 + 1);
				auto& Pixel3 = MipNext.At(x * 2 + 1, y * 2 + 0);
				auto& Pixel4 = MipNext.At(x * 2 + 1, y * 2 + 1);

				Pixel.HeightMax = FMath::Max(FMath::Max(Pixel1.HeightMax, Pixel2.HeightMax), FMath::Max(Pixel3.HeightMax, Pixel4.HeightMax));
				Pixel.HeightMin = FMath::Min(FMath::Min(Pixel1.HeightMin, Pixel2.HeightMin), FMath::Min(Pixel3.HeightMin, Pixel4.HeightMin));
			}
	}

	TFunction<FBlockType(FIntVector const& Pos)> GenFunc = [&](FIntVector const& Pos) {
		auto LandPixel = LandData.Pixels.At({ Pos.X + 1, Pos.Y + 1 });
		auto PosGlobal = Pos + PosChunkBase;
		if (PosGlobal.Z <= LandPixel.Height)
		{
			if (LandPixel.Biome == EBiome::DeepOcean)
			{
				return BlockTypeSand;
			}
			if (LandPixel.Biome == EBiome::Ocean)
			{
				return BlockTypeSand;
			}
			if (LandPixel.Biome == EBiome::ShallowWater)
			{
				return BlockTypeSand;
			}
			if (LandPixel.Biome == EBiome::WaterSandBeach)
			{
				return BlockTypeSand;
			}
			if (LandPixel.Biome == EBiome::SandBeach)
			{
				return BlockTypeSand;
			}
			if (LandPixel.Biome == EBiome::GrassPlains)
			{
				return BlockTypeGrass;
			}
			if (LandPixel.Biome == EBiome::Forest)
			{
				return BlockTypeGrass;
			}
			if (LandPixel.Biome == EBiome::Mountains)
			{
				return BlockTypeStone;
			}
			if (LandPixel.Biome == EBiome::HighMountains)
			{
				return BlockTypeSnow;
			}
		}
		else if (PosGlobal.Z < 0)
		{
			return BlockTypeWater;
		}
		return BlockTypeAir;
	};

	TFunction<TOptional<FBlockType>(FIntVector const& Pos, int Level)> GenCubeInfo =
		[&](FIntVector const& Pos, int Level) -> TOptional<FBlockType> {
		int CubeSize = FVoxChunk::LevelCubeSize(Level);

		int PosZGlobMin = PosChunkBase.Z + Pos.Z * CubeSize;
		int PosZGlobMax = PosChunkBase.Z + (Pos.Z + 1) * CubeSize - 1;

		auto MipPixel = LandMips[Level].At(Pos.X, Pos.Y);

		if (PosZGlobMin > MipPixel.HeightMax)
		{
			if (PosZGlobMin >= 0)
				return BlockTypeAir;
			if (PosZGlobMax < 0)
				return BlockTypeWater;
		}
		if (PosZGlobMax < MipPixel.HeightMin)
		{
			return BlockTypeNgen;
		}
		return {};
	};

	Chunk->InitAdvanced(GenFunc, GenCubeInfo);
	// Chunk->DebugRenderVoxels(FVoxChunk::TreeLevels);
	// Chunk->DebugRenderSides();
	return Chunk;
}

TSharedPtr<FVoxChunk> FChunkGenerator::GenChunkSimple(FIntVector const& ChunkPos)
{
	PROFILE_FUNCTION()
	auto Chunk = MakeShared<FVoxChunk>();

	// FIntVector PosChunkBase = ChunkPos * FVoxChunk::ChunkSize;
	//
	// FLandRectData LandData;
	// LandData.PosBase = FIntVector2(PosChunkBase.X - 1, PosChunkBase.Y - 1);
	// LandData.Pixels.SetSize({ FVoxChunk::ChunkSize + 2, FVoxChunk::ChunkSize + 2 });
	//
	// LandGenerator->GetLandRect(LandData);
	//
	// TFunction<FBlockType(FIntVector const& Pos)> GenFunc = [&](FIntVector const& Pos) {
	// 	auto LandPixel = LandData.Pixels.At({ Pos.X + 1, Pos.Y + 1 });
	// 	auto PosGlobal = Pos + PosChunkBase;
	// 	if (PosGlobal.Z < LandPixel.Height)
	// 	{
	// 		if (LandPixel.Biome == EBiome::DeepOcean)
	// 		{
	// 			return BlockTypeSand;
	// 		}
	// 		if (LandPixel.Biome == EBiome::Ocean)
	// 		{
	// 			return BlockTypeSand;
	// 		}
	// 		if (LandPixel.Biome == EBiome::ShallowWater)
	// 		{
	// 			return BlockTypeSand;
	// 		}
	// 		if (LandPixel.Biome == EBiome::WaterSandBeach)
	// 		{
	// 			return BlockTypeSand;
	// 		}
	// 		if (LandPixel.Biome == EBiome::SandBeach)
	// 		{
	// 			return BlockTypeSand;
	// 		}
	// 		if (LandPixel.Biome == EBiome::GrassPlains)
	// 		{
	// 			return BlockTypeGrass;
	// 		}
	// 		if (LandPixel.Biome == EBiome::Forest)
	// 		{
	// 			return BlockTypeGrass;
	// 		}
	// 		if (LandPixel.Biome == EBiome::Mountains)
	// 		{
	// 			return BlockTypeStone;
	// 		}
	// 		if (LandPixel.Biome == EBiome::HighMountains)
	// 		{
	// 			return BlockTypeSnow;
	// 		}
	// 	}
	// 	return BlockTypeAir;
	// };
	//
	// Chunk->Init(GenFunc);

	return Chunk;
}

TSharedPtr<FVoxChunk> ChunkTestSphereGen()
{
	const siv::PerlinNoise::seed_type Seed = 123456u;

	const siv::PerlinNoise Perlin{ Seed };

	TArrayDim3<FBlockType> Data;
	Data.SetSize({ FVoxChunk::ChunkSize + 2, FVoxChunk::ChunkSize + 2, FVoxChunk::ChunkSize + 2 });

	FVector PosCentre(FVoxChunk::ChunkSize / 2.0 + 0.5, FVoxChunk::ChunkSize / 2.0 + 0.5, FVoxChunk::ChunkSize / 2.0 + 0.5);

	for (int x = 0; x < FVoxChunk::ChunkSize + 2; x++)
		for (int y = 0; y < FVoxChunk::ChunkSize + 2; y++)
			for (int z = 0; z < FVoxChunk::ChunkSize + 2; z++)
			{
				FIntVector Pos(x, y, z);

				double Height = FVoxChunk::ChunkSize / 2 + Perlin.octave2D(x * 0.01, y * 0.01, 3) * 10;

				int HeightLand = FMath::Floor(z - Height);

				if (HeightLand <= 0)
				{
					if (HeightLand == 0)
					{
						Data.At({ x, y, z }) = BlockTypeGrass;
					}
					else if (HeightLand > -5)
					{
						Data.At({ x, y, z }) = BlockTypeDirt;
					}
					else
					{
						Data.At({ x, y, z }) = BlockTypeStone;
					}
				}

				auto Diff = PosCentre - FVector(Pos);
				auto DiffCube = FMath::Max3(abs(Diff.X), abs(Diff.Y), abs(Diff.Z));

				// if (DiffCube > 125)
				if (FVector::Distance(PosCentre, FVector(Pos)) > FVoxChunk::ChunkSize / 2 - 3)
				{
					Data.At({ x, y, z }) = BlockTypeAir;
				}
			}

	TFunction<FBlockType(FIntVector const& Pos)> GenFunc = [&](FIntVector const& Pos) {
		return Data.At({ Pos.X + 1, Pos.Y + 1, Pos.Z + 1 });
	};

	auto Chunk = MakeShared<FVoxChunk>();
	Chunk->Init(GenFunc);

	return Chunk;
}

TSharedPtr<FVoxChunk> ChunkTestCubicGen()
{
	const siv::PerlinNoise::seed_type Seed = 123456u;

	const siv::PerlinNoise Perlin{ Seed };

	TArrayDim3<FBlockType> Data;
	Data.SetSize({ FVoxChunk::ChunkSize + 2, FVoxChunk::ChunkSize + 2, FVoxChunk::ChunkSize + 2 });

	FVector PosCentre(FVoxChunk::ChunkSize / 2.0 + 0.5, FVoxChunk::ChunkSize / 2.0 + 0.5, FVoxChunk::ChunkSize / 2.0 + 0.5);

	for (int x = 0; x < FVoxChunk::ChunkSize + 2; x++)
		for (int y = 0; y < FVoxChunk::ChunkSize + 2; y++)
			for (int z = 0; z < FVoxChunk::ChunkSize + 2; z++)
			{
				FIntVector Pos(x, y, z);

				double Height = FVoxChunk::ChunkSize / 2 + Perlin.octave2D(x * 0.01, y * 0.01, 3) * 10;

				int HeightLand = FMath::Floor(z - Height);

				if (HeightLand <= 0)
				{
					if (HeightLand == 0)
					{
						Data.At({ x, y, z }) = BlockTypeGrass;
					}
					else if (HeightLand > -5)
					{
						Data.At({ x, y, z }) = BlockTypeDirt;
					}
					else
					{
						Data.At({ x, y, z }) = BlockTypeStone;
					}
				}
			}

	TFunction<FBlockType(FIntVector const& Pos)> GenFunc = [&](FIntVector const& Pos) {
		return Data.At({ Pos.X + 1, Pos.Y + 1, Pos.Z + 1 });
	};

	auto Chunk = MakeShared<FVoxChunk>();
	Chunk->Init(GenFunc);

	return Chunk;
}
TSharedPtr<FVoxChunk> ChunkTestVox()
{
	FString FilePath = "C:\\Users\\nikit\\Downloads\\untitled.vox";

	const siv::PerlinNoise::seed_type Seed = 123456u;

	const siv::PerlinNoise Perlin{ Seed };

	TArrayDim3<FBlockType> Data;
	Data.SetSize({ FVoxChunk::ChunkSize + 2, FVoxChunk::ChunkSize + 2, FVoxChunk::ChunkSize + 2 });

	auto VoxData = LoadVoxFile(FilePath);

	for (int x = 0; x < FVoxChunk::ChunkSize; x++)
		for (int y = 0; y < FVoxChunk::ChunkSize; y++)
			for (int z = 0; z < FVoxChunk::ChunkSize; z++)
			{
				if (x < VoxData.Voxels.Num().X)
					if (y < VoxData.Voxels.Num().Y)
						if (z < VoxData.Voxels.Num().Z)
							Data.At({ x + 1, y + 1, z + 1 }) = FBlockType{ VoxData.Voxels.At(x, y, z) };
			}

	TFunction<FBlockType(FIntVector const& Pos)> GenFunc = [&](FIntVector const& Pos) {
		return Data.At({ Pos.X + 1, Pos.Y + 1, Pos.Z + 1 });
	};

	TFunction<TOptional<FBlockType>(FIntVector const& Pos, int Level)> GenCubeInfo =
		[&](FIntVector const& Pos, int Level) -> TOptional<FBlockType> {
		return {};
	};

	auto Chunk = MakeShared<FVoxChunk>();
	Chunk->InitAdvanced(GenFunc, GenCubeInfo);

	return Chunk;
}