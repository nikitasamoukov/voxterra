﻿#pragma once

#include "VoxTerra/World/Terrain/VoxChunk.h"

struct FLandGenerator;

struct FChunkGenerator
{
	explicit FChunkGenerator(int Seed = 1337);

	TSharedPtr<FVoxChunk> GenChunkAdvanced(FIntVector const& ChunkPos);
	TSharedPtr<FVoxChunk> GenChunkSimple(FIntVector const& ChunkPos);

	TSharedPtr<FLandGenerator> LandGenerator;
};

TSharedPtr<FVoxChunk> ChunkTestSphereGen();
TSharedPtr<FVoxChunk> ChunkTestCubicGen();
TSharedPtr<FVoxChunk> ChunkTestVox();
