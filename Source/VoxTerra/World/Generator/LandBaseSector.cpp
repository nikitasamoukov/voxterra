﻿#include "LandBaseSector.h"

#include "ProceduralMeshComponent.h"
#include "VoxTerra/Common/TextureUtil.h"
#include "VoxTerra/World/Terrain/VoxChunk.h"

#include <PerlinNoise/PerlinNoise.hpp>

FLandBaseSector::FLandBaseSector()
{
	Texture.SetSize({ SectorSize, SectorSize });
	// TextureLowRes.SetSize({ TextureLowResSize, TextureLowResSize });
}

void FLandBaseSector::Generate(TSharedPtr<FLandBaseGenerator> const& Gen, FIntVector2 const& SectorPosNew)
{
	PROFILE_FUNCTION()
	SectorPos = SectorPosNew;

	for (int x = 0; x < SectorSize; x++)
		for (int y = 0; y < SectorSize; y++)
		{
			auto Pixel = Gen->GetPixel({ x + SectorPos.X * SectorSize, y + SectorPos.Y * SectorSize });

			Texture.At(x, y) = Pixel;
		}
}

void FLandBaseSector::GetDebugInfo(FDebugOutput& Res)
{
	{
		FImageCpuRgba8 TextureCpu;
		TextureCpu.Init(SectorSize, SectorSize);

		for (int x = 0; x < SectorSize; x++)
			for (int y = 0; y < SectorSize; y++)
			{
				auto& Pixel = Texture.At({ x, y });

				auto Biome = Pixel.Biome;

				auto BiomeInfo = GBiomesInfo.Find(Biome);
				FColor Color = FColor::Black;
				if (BiomeInfo)
				{
					Color = BiomeInfo->Color;
				}

				TextureCpu.At(x, y) = Color;
			}

		Res.Texture = ConvCpuTextureToTexture2d(TextureCpu);
	}
	{
		FImageCpuRgba8 TextureCpu;
		TextureCpu.Init(SectorSize, SectorSize);

		for (int x = 0; x < SectorSize; x++)
			for (int y = 0; y < SectorSize; y++)
			{
				auto& Pixel = Texture.At({ x, y });

				float C = Pixel.Height / 500.0 + 0.5;
				C = FMath::Clamp(C, 0, 1);

				FColor Color = FColor(C * 255, C * 255, C * 255, 255);

				if (Pixel.Height >= 0 && Pixel.Height < 5)
				{
					Color = FColor(255, 255, 255, 255);
				}

				TextureCpu.At(x, y) = Color;
			}

		Res.Texture2 = ConvCpuTextureToTexture2d(TextureCpu);
	}

	if (Res.MeshComponent && 0)
	{
		FChunkMeshData MeshData;

		for (int x = 0; x < SectorSize - 1; x++)
			for (int y = 0; y < SectorSize - 1; y++)
			{
				auto GetP = [&](FIntVector2 const& Pos) {
					return FVector(Pos.X, Pos.Y, Texture.At({ Pos.X, Pos.Y }).Height);
				};

				int IdxStart = MeshData.Vertices.Num();

				MeshData.Vertices.Add(GetP({ x, y }));
				MeshData.Vertices.Add(GetP({ x + 1, y }));
				MeshData.Vertices.Add(GetP({ x + 1, y + 1 }));
				MeshData.Vertices.Add(GetP({ x, y + 1 }));

				MeshData.Normals.Add({});
				MeshData.Normals.Add({});
				MeshData.Normals.Add({});
				MeshData.Normals.Add({});

				MeshData.UV.Add({});
				MeshData.UV.Add({});
				MeshData.UV.Add({});
				MeshData.UV.Add({});

				MeshData.Colors.Add({});
				MeshData.Colors.Add({});
				MeshData.Colors.Add({});
				MeshData.Colors.Add({});

				MeshData.Triangles.Add(IdxStart + 0);
				MeshData.Triangles.Add(IdxStart + 1);
				MeshData.Triangles.Add(IdxStart + 2);
				MeshData.Triangles.Add(IdxStart + 0);
				MeshData.Triangles.Add(IdxStart + 2);
				MeshData.Triangles.Add(IdxStart + 3);
			}

		Res.MeshComponent->CreateMeshSection(
			0,
			MeshData.Vertices,
			MeshData.Triangles,
			MeshData.Normals,
			MeshData.UV,
			MeshData.Colors,
			TArray<FProcMeshTangent>(),
			true);
	}
}