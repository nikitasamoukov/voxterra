﻿#include "BiomesInfo.h"

TMap<EBiome, FBiomeInfo> GBiomesInfo = [] {
	TMap<EBiome, FBiomeInfo> Res;

	Res.Add(EBiome::DeepOcean,
		FBiomeInfo{
			FColor::FromHex("000481") });
	Res.Add(EBiome::Ocean,
		FBiomeInfo{
			FColor::FromHex("0004FF") });
	Res.Add(EBiome::ShallowWater,
		FBiomeInfo{
			FColor::FromHex("00C7FF") });
	Res.Add(EBiome::WaterSandBeach,
		FBiomeInfo{
			FColor::FromHex("75BA80") });
	Res.Add(EBiome::SandBeach,
		FBiomeInfo{
			FColor::FromHex("FFED7A") });
	Res.Add(EBiome::GrassPlains,
		FBiomeInfo{
			FColor::FromHex("00B51E") });
	Res.Add(EBiome::Forest,
		FBiomeInfo{
			FColor::FromHex("005E0E") });
	Res.Add(EBiome::Mountains,
		FBiomeInfo{
			FColor::FromHex("5B5B5B") });
	Res.Add(EBiome::HighMountains,
		FBiomeInfo{
			FColor::FromHex("BCBCBC") });

	return Res;
}();