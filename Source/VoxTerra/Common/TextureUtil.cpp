﻿#include "TextureUtil.h"

#include "ImageUtils.h"

UTexture2D* ConvCpuTextureToTexture2d(FImageCpuRgba8 const& TextureCpu)
{
	FCreateTexture2DParameters Params;
	
	return FImageUtils::CreateTexture2D(
			TextureCpu.W, TextureCpu.H, TextureCpu.Data,
			GetTransientPackage(),FString(), EObjectFlags::RF_NoFlags, Params);
}

