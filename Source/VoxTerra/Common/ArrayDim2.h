﻿#pragma once

template <typename T>
class TArrayDim2
{
public:
	explicit TArrayDim2(FIntVector2 const& SizeNew = { 0, 0 });
	void SetSize(FIntVector2 const& SizeNew = { 0, 0 });

	auto& Num() const { return ArraySize; }
	auto& RawData() const { return ArrayRawData; }
	T& At(FIntVector2 const& Idx) { return ArrayRawData[Idx.X + Idx.Y * ArraySize.X]; }
	T& At(int X, int Y) { return At({ X, Y }); }

private:
	TArray<T> ArrayRawData;
	FIntVector2 ArraySize;
};

template <typename T>
TArrayDim2<T>::TArrayDim2(FIntVector2 const& SizeNew)
{
	SetSize(SizeNew);
}

template <typename T>
void TArrayDim2<T>::SetSize(FIntVector2 const& SizeNew)
{
	ArraySize = SizeNew;

	ArrayRawData.SetNum(ArraySize.X * ArraySize.Y);
}