﻿#include "GameChunk.h"

#include "ProceduralMeshComponent.h"
#include "VoxChunk.h"

AGameChunk::AGameChunk()
{
	Mesh = CreateDefaultSubobject<UProceduralMeshComponent>("Mesh");
	SetRootComponent(Mesh);

	Mesh->bUseAsyncCooking = true;
}

void AGameChunk::BeginPlay()
{
	Super::BeginPlay();
}

AGameChunk* AGameChunk::Create(
	UWorld* World,
	FTransform const& Transform,
	AVoxTerraWorld* VoxTerrainNew,
	TSharedPtr<FVoxChunk> const& VoxChunkNew)
{
	auto Actor = World->SpawnActorDeferred<AGameChunk>(AGameChunk::StaticClass(), Transform);
	Actor->Init(VoxTerrainNew, VoxChunkNew);
	Actor->FinishSpawning(Transform);

	return Actor;
}

void AGameChunk::Init(AVoxTerraWorld* VoxTerrainNew, TSharedPtr<FVoxChunk> const& VoxChunkNew)
{
	VoxTerrain = VoxTerrainNew;
	VoxChunk = VoxChunkNew;
}

void AGameChunk::UpdateMesh()
{
	if (VoxChunk->MeshDataSolid)
	{
		auto& MeshData = *VoxChunk->MeshDataSolid;

		Mesh->CreateMeshSection(
			0,
			MeshData.Vertices,
			MeshData.Triangles,
			MeshData.Normals,
			MeshData.UV,
			MeshData.Colors,
			TArray<FProcMeshTangent>(),
			true);
		MeshData.Clear();
	}
	if (VoxChunk->MeshDataTranslucent)
	{
		auto& MeshData = *VoxChunk->MeshDataTranslucent;

		Mesh->CreateMeshSection(
			1,
			MeshData.Vertices,
			MeshData.Triangles,
			MeshData.Normals,
			MeshData.UV,
			MeshData.Colors,
			TArray<FProcMeshTangent>(),
			true);
		MeshData.Clear();
	}
	if (VoxChunk->MeshDataWater)
	{
		auto& MeshData = *VoxChunk->MeshDataWater;

		Mesh->CreateMeshSection(
			2,
			MeshData.Vertices,
			MeshData.Triangles,
			MeshData.Normals,
			MeshData.UV,
			MeshData.Colors,
			TArray<FProcMeshTangent>(),
			false);
		MeshData.Clear();
	}
}

bool AGameChunk::IsMeshReady()
{
	return (bool)Mesh->ProcMeshBodySetup;
}