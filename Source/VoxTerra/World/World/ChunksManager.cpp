﻿#include "ChunksManager.h"

#include "VoxTerra/World/Generator/ChunkGenerator.h"
#include "VoxTerra/World/Terrain/VoxChunk.h"

FChunksManager::FChunksManager()
{
	ChunkGenerator = MakeShared<FChunkGenerator>();
}

FChunksManager::~FChunksManager()
{
}

void FChunksManager::Update(FVector const& PlayerPos, double DistanceLoad, double DistanceUnload)
{
	PROFILE_FUNCTION()
	auto ChunksToLoad = GetChunksToLoad(PlayerPos, DistanceLoad);

	FScopeLock Lock(&Mutex);

	for (auto& ChunksPos : ChunksToLoad)
	{
		if (!Chunks.Contains(ChunksPos))
		{
			auto& Chunk = Chunks.Add(ChunksPos);

			Chunk.bNeedLoad = true;
			Chunk.ChunkPos = ChunksPos;
		}
		auto Cell = Chunks.Find(ChunksPos);

		Cell->bNeedLoad = true;
	}

	for (auto& Pair : Chunks)
	{
		auto ChunkPos = Pair.Key;
		auto ChunkCenter = FVoxChunk::GetSectorCenter(ChunkPos);
		auto Dist = FVector::Distance(PlayerPos, ChunkCenter);
		if (Dist > DistanceUnload + FVoxChunk::ChunkSphereSize)
		{
			Pair.Value.bNeedLoad = false;
		}
	}

	Update();
}

TArray<TPair<FIntVector, TSharedPtr<FVoxChunk>>> FChunksManager::GetChunks()
{
	PROFILE_FUNCTION()
	FScopeLock Lock(&Mutex);
	TArray<TPair<FIntVector, TSharedPtr<FVoxChunk>>> Res;

	for (auto& Pair : Chunks)
	{
		if (Pair.Value.Chunk)
		{
			Res.Add({ Pair.Key, Pair.Value.Chunk });
		}
	}

	return Res;
}

void FChunksManager::Update()
{
	PROFILE_FUNCTION()
	for (auto& Pair : Chunks)
	{
		auto& Cell = Pair.Value;
		if (Cell.bNeedLoad && Cell.State == EChunkState::Nothing)
		{
			RequestLoadCell(Cell);
		}
	}
}

void FChunksManager::RequestLoadCell(FChunkCell& Cell)
{
	PROFILE_FUNCTION()
	if (Cell.State != EChunkState::Nothing)
	{
		LOG_ERROR();
		return;
	}

	Cell.State = EChunkState::Generating;

	TFunction<TSharedPtr<FVoxChunk>()> Task = [ChunkGenerator = ChunkGenerator, ChunkPos = Cell.ChunkPos]() -> TSharedPtr<FVoxChunk> {
		PROFILE_SCOPE("Task generate chunk")
		auto Chunk = ChunkGenerator->GenChunkAdvanced(ChunkPos);
		if (Chunk)
		{
			Chunk->GenMeshData();
		}

		return Chunk;
	};
	TFunction<void()> OnTaskFinish = [Manager = this->AsShared(), ChunkPos = Cell.ChunkPos]() {
		Manager->OnFinishLoadChunk(ChunkPos);
	};

	Cell.LoadFuture = Async(EAsyncExecution::ThreadPool, Task, OnTaskFinish);
}

void FChunksManager::OnFinishLoadChunk(FIntVector const& ChunkPos)
{
	PROFILE_FUNCTION()
	FScopeLock Lock(&Mutex);

	auto Cell = Chunks.Find(ChunkPos);

	if (!Cell)
	{
		LOG_ERROR();
		return;
	}
	if (Cell->State != EChunkState::Generating && Cell->State != EChunkState::LoadFromHdd)
	{
		LOG_ERROR();
		return;
	}
	if (!Cell->LoadFuture.IsReady())
	{
		LOG_ERROR();
		return;
	}

	Cell->Chunk = Cell->LoadFuture.Get();
	if (Cell->Chunk)
	{
		Cell->State = EChunkState::Ready;
	}
	else
	{
		Cell->State = EChunkState::Nothing;
	}
	Cell->LoadFuture.Reset();
}

TArray<FIntVector> FChunksManager::GetChunksToLoad(FVector const& PlayerPos, double DistanceLoad)
{
	TArray<FIntVector> Res;

	int BlocksCubeDist = DistanceLoad / FVoxChunk::ChunkSize + 2;
	FIntVector CurrentChunkPos = FIntVector(
		FMath::Floor(PlayerPos.X / FVoxChunk::ChunkSize),
		FMath::Floor(PlayerPos.Y / FVoxChunk::ChunkSize),
		FMath::Floor(PlayerPos.Z / FVoxChunk::ChunkSize));

	double DistChunkMax = DistanceLoad + FVoxChunk::ChunkSphereSize;

	FIntVector Offset = FIntVector::ZeroValue;
	for (Offset.X = -BlocksCubeDist; Offset.X <= BlocksCubeDist; Offset.X++)
		for (Offset.Y = -BlocksCubeDist; Offset.Y <= BlocksCubeDist; Offset.Y++)
			for (Offset.Z = -BlocksCubeDist; Offset.Z <= BlocksCubeDist; Offset.Z++)
			{
				FIntVector ChunkPos = CurrentChunkPos + Offset;
				auto ChunkCenter = FVoxChunk::GetSectorCenter(ChunkPos);
				auto Dist = FVector::Distance(PlayerPos, ChunkCenter);
				if (Dist <= DistChunkMax)
				{
					Res.Add(ChunkPos);
				}
			}

	return Res;
}