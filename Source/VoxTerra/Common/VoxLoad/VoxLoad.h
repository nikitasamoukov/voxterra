#pragma once
#include "VoxTerra/Common/ArrayDim3.h"

struct FVoxModel
{
	TArrayDim3<uint8> Voxels;
	TArray<FColor> Palette;
};

FVoxModel LoadVoxFile(FString FilePath);