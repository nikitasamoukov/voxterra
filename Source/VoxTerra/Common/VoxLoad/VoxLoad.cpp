#include "VoxLoad.h"

#define OGT_VOX_IMPLEMENTATION
#include "ogt_vox.h"

// a helper function to load a magica voxel scene given a filename.
const ogt_vox_scene* load_vox_scene(const char* filename, uint32_t scene_read_flags = 0)
{
// open the file
#if defined(_MSC_VER) && _MSC_VER >= 1400
	FILE* fp;
	if (0 != fopen_s(&fp, filename, "rb"))
		fp = 0;
#else
	FILE* fp = fopen(filename, "rb");
#endif
	if (!fp)
		return NULL;

	// get the buffer size which matches the size of the file
	fseek(fp, 0, SEEK_END);
	uint32_t buffer_size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	// load the file into a memory buffer
	uint8_t* buffer = new uint8_t[buffer_size];
	fread(buffer, buffer_size, 1, fp);
	fclose(fp);

	// construct the scene from the buffer
	const ogt_vox_scene* scene = ogt_vox_read_scene_with_flags(buffer, buffer_size, scene_read_flags);

	// the buffer can be safely deleted once the scene is instantiated.
	delete[] buffer;

	return scene;
}

FVoxModel LoadVoxFile(FString FilePath)
{
	FVoxModel Res;

	auto Scene = load_vox_scene(TCHAR_TO_UTF8(*FilePath));

	if (!Scene)
		return Res;
	if (Scene->num_models != 1)
	{
		LOG_ERROR(".vox error: models count not 1");
		return Res;
	}

	auto Model = Scene->models[0];

	Res.Voxels.SetSize(FIntVector(Model->size_x, Model->size_y, Model->size_z));
	for (int x = 0; x < (int)Model->size_x; x++)
		for (int y = 0; y < (int)Model->size_y; y++)
			for (int z = 0; z < (int)Model->size_z; z++)
			{
				Res.Voxels.At(x, y, z) = Model->voxel_data[x + y * Model->size_x + z * Model->size_x * Model->size_y];
			}

	Res.Palette.SetNum(256);
	for (int i = 0; i < 256; i++)
		Res.Palette[i] = FColor(Scene->palette.color[i].r, Scene->palette.color[i].g, Scene->palette.color[i].b, Scene->palette.color[i].a);

	ogt_vox_destroy_scene(Scene);

	return Res;
}