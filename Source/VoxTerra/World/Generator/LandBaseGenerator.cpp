﻿#include "LandBaseGenerator.h"

#include <PerlinNoise/PerlinNoise.hpp>

struct FLandBaseGenerator::FImpl
{
	explicit FImpl(siv::PerlinNoise::seed_type Seed)
		: Noise(Seed)
	{
	}

	FLandBasePixel GetPixel(FIntVector2 const& Pos)
	{
		double MultNoiseMainSize = 0.001;

		FLandBasePixel Pixel;

		double NoiseMain = Noise.octave2D(
			(Pos.X) * MultNoiseMainSize,
			(Pos.Y) * MultNoiseMainSize,
			6);

		float Height = (NoiseMain - 0.2) * 1000;

		EBiome Biome = EBiome::Unknown;
		if (Height < -1000)
		{
			Biome = EBiome::DeepOcean;
		}
		else if (Height < -150)
		{
			Biome = EBiome::Ocean;
		}
		else if (Height < -30)
		{
			Biome = EBiome::ShallowWater;
		}
		else if (Height < -1)
		{
			Biome = EBiome::WaterSandBeach;
		}
		else if (Height < 30)
		{
			Biome = EBiome::SandBeach;
		}
		else if (Height < 100)
		{
			Biome = EBiome::GrassPlains;
		}
		else if (Height < 1000)
		{
			Biome = EBiome::Forest;
		}
		else if (Height < 1500)
		{
			Biome = EBiome::Mountains;
		}
		else
		{
			Biome = EBiome::HighMountains;
		}

		Pixel.Biome = Biome;
		Pixel.Height = Height / 10;
		//Pixel.Height = 2;

		return Pixel;
	}

	const siv::PerlinNoise Noise;
};

FLandBaseGenerator::FLandBaseGenerator(int32 SeedNew)
{
	Impl = MakeUnique<FImpl>(SeedNew);
}
FLandBaseGenerator::~FLandBaseGenerator()
{
}
FLandBasePixel FLandBaseGenerator::GetPixel(FIntVector2 const& Pos) const
{
	return Impl->GetPixel(Pos);
}