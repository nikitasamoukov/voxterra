﻿#pragma once

struct FChunkGenerator;
struct FVoxChunk;

struct FChunksManager : public TSharedFromThis<FChunksManager>
{
	enum class EChunkState
	{
		Nothing,
		Ready,

		Loading,
		Generating,
		LoadFromHdd,
		SaveToHdd,

		PendingDestruct,
	};

	struct FChunkCell
	{
		EChunkState State = EChunkState::Nothing;
		bool bNeedLoad = false;

		TFuture<TSharedPtr<FVoxChunk>> LoadFuture;

		FIntVector ChunkPos;
		TSharedPtr<FVoxChunk> Chunk;
	};

	FChunksManager();
	~FChunksManager();

	void Update(FVector const& PlayerPos, double DistanceLoad, double DistanceUnload);
	TArray<TPair<FIntVector, TSharedPtr<FVoxChunk>>> GetChunks();

private:
	void Update();
	void RequestLoadCell(FChunkCell& Cell);
	void OnFinishLoadChunk(FIntVector const& ChunkPos);
	TArray<FIntVector> GetChunksToLoad(FVector const& PlayerPos, double DistanceLoad);

	TSharedPtr<FChunkGenerator> ChunkGenerator;
	FCriticalSection Mutex;
	TMap<FIntVector, FChunkCell> Chunks;
};