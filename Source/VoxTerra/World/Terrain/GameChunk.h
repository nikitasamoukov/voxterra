﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameChunk.generated.h"

class UProceduralMeshComponent;
struct FVoxChunk;
class AVoxTerraWorld;

UCLASS()
class VOXTERRA_API AGameChunk : public AActor
{
	GENERATED_BODY()

public:
	AGameChunk();

	virtual void BeginPlay() override;

	static AGameChunk* Create(
		UWorld* World,
		FTransform const& Transform,
		AVoxTerraWorld* VoxTerrainNew,
		TSharedPtr<FVoxChunk> const& VoxChunkNew);
	void Init(AVoxTerraWorld* VoxTerrainNew, TSharedPtr<FVoxChunk> const& VoxChunkNew);
	
	void UpdateMesh();
	bool IsMeshReady();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UProceduralMeshComponent* Mesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	AVoxTerraWorld* VoxTerrain = nullptr;

	TSharedPtr<FVoxChunk> VoxChunk;
};