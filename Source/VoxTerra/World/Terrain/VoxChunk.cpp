﻿#include "VoxChunk.h"

#include "BlockTypeInfo.h"
#include "VoxTerra/Common/ArrayDim3.h"
#include "VoxTerra/Common/LogUtil.h"

TArray<FIntVector> AxisVec = {
	{ 1, 0, 0 },
	{ 0, 1, 0 },
	{ 0, 0, 1 },
};

TArray<FVoxSide::FLeafInfo> FVoxSide::GetAllVoxels()
{
	PROFILE_FUNCTION()
	TArray<FLeafInfo> Res;

	if (Nodes.Num() == 0)
	{
		FLeafInfo Info;
		Info.Block = *Block;
		Info.Level = 0;
		Info.Pos = { 0, 0 };
		Res.Add(Info);
		return Res;
	}

	TFunction<void(FIntVector2 Pos, int Level, int Idx)> Traverser = [&](FIntVector2 Pos, int Level, int Idx) {
		auto& CurrentNode = Nodes[Idx];

		for (int x = 0; x < 2; x++)
			for (int y = 0; y < 2; y++)
			{
				auto& ChildInfoData = CurrentNode.Children[x][y];
				FIntVector2 PosChild(Pos.X * 2 + x, Pos.Y * 2 + y);

				auto ChildInfo = ChildInfoData.GetInfo();
				if (ChildInfo.Key == ERefType::Node)
				{
					Traverser(PosChild, Level + 1, ChildInfo.Value);
				}
				if (ChildInfo.Key == ERefType::Leaf)
				{
					FBlockType Block = { ChildInfo.Value };
					// if (Block != BlockTypeAir && Block != BlockTypeNgen)
					{
						if (Level + 1 >= FVoxChunk::TreeLevels)
						{
							LOG_ERROR("Voxel on incorrect level:" + FString::FromInt(Level) + " Max:" + FString::FromInt(FVoxChunk::TreeLevels - 1));
						}
						FLeafInfo Info = { .Pos = PosChild, .Block = Block, .Level = Level + 1 };

						Res.Add(Info);
					}
				}
			}
	};
	Traverser(FIntVector2(0, 0), 0, 0);

	return Res;
}

FVoxSide::FLeafInfo FVoxSide::AtAdv(FIntVector2 const& PosSample)
{
	FLeafInfo Res;

	if (Nodes.Num() == 0)
	{
		Res.Pos = { 0, 0 };
		Res.Block = *Block;
		Res.Level = 0;
		return Res;
	}

	FIntVector2 Pos(0, 0);

	int CurrNodeIdx = 0;

	for (int Level = 0; Level < FVoxChunk::TreeLevels - 1; Level++)
	{
		auto& Node = Nodes[CurrNodeIdx];

		uint32 BitMask = uint32(1) << (FVoxChunk::TreeLevels - 2 - Level);

		int x = (PosSample.X & BitMask) ? 1 : 0;
		int y = (PosSample.Y & BitMask) ? 1 : 0;

		FIntVector2 PosChild(Pos.X * 2 + x, Pos.Y * 2 + y);
		Pos = PosChild;

		auto Info = Node.Children[x][y].GetInfo();
		if (Info.Key == ERefType::Node)
		{
			CurrNodeIdx = Info.Value;
		}
		else
		{
			Res.Pos = Pos;
			Res.Block = FBlockType{ Info.Value };
			Res.Level = Level + 1;
			return Res;
		}
	}
	LOG_ERROR();
	return Res;
}

FVoxChunk::FVoxChunk()
{
}

void FVoxChunk::Init(TFunction<FBlockType(FIntVector const& Pos)> const& GenFunc)
{
	PROFILE_FUNCTION()
	FWorldContext* WorldC = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
	UWorld* World = WorldC ? WorldC->World() : nullptr;

	Nodes.SetNum(0);

	TArrayDim3<FBlockType> BlocksTypeFull({ ChunkSize + 2, ChunkSize + 2, ChunkSize + 2 });

	{
		PROFILE_SCOPE("FVoxChunk::Init 1")
		for (int x = 0; x < ChunkSize + 2; x++)
			for (int y = 0; y < ChunkSize + 2; y++)
				for (int z = 0; z < ChunkSize + 2; z++)
				{
					BlocksTypeFull.At({ x, y, z }) = GenFunc({ x - 1, y - 1, z - 1 });
				}
	}

	TArray<FIntVector> NeighborsOffsets = {
		{ 1, 0, 0 },
		{ -1, 0, 0 },
		{ 0, 1, 0 },
		{ 0, -1, 0 },
		{ 0, 0, 1 },
		{ 0, 0, -1 }
	};

	{
		PROFILE_SCOPE("FVoxChunk::Init 2")
		for (int x = 1; x < ChunkSize + 1; x++)
			for (int y = 1; y < ChunkSize + 1; y++)
				for (int z = 1; z < ChunkSize + 1; z++)
				{
					FIntVector Pos(x, y, z);

					if (BlocksTypeFull.At(Pos).IsAir())
						continue;

					bool bHaveAir = false;
					for (auto& Offset : NeighborsOffsets)
					{
						if (BlocksTypeFull.At(Pos + Offset).IsAir())
							bHaveAir = true;
					}

					if (bHaveAir)
						continue;

					BlocksTypeFull.At(Pos) = BlockTypeNgen;
				}
	}

	TArray<TArrayDim3<EBlockTreeType>> TreeInfo;

	for (int Level = 0; Level < TreeLevels - 1; Level++)
	{
		int Size = 1 << Level;
		TreeInfo.Emplace(FIntVector(Size, Size, Size));
	}

	TArray<FIntVector> ChildrenOffsets = {
		{ 0, 0, 0 },
		{ 0, 0, 1 },
		{ 0, 1, 0 },
		{ 0, 1, 1 },
		{ 1, 0, 0 },
		{ 1, 0, 1 },
		{ 1, 1, 0 },
		{ 1, 1, 1 }
	};

	{
		PROFILE_SCOPE("FVoxChunk::Init 3")

		int Level = TreeLevels - 2;
		int Size = 1 << Level;

		for (int x = 0; x < Size; x++)
			for (int y = 0; y < Size; y++)
				for (int z = 0; z < Size; z++)
				{
					FIntVector PosBase(x * 2 + 1, y * 2 + 1, z * 2 + 1);

					int CountAir = 0;
					int CountNgen = 0;

					for (auto& Offset : ChildrenOffsets)
					{
						FIntVector Pos = PosBase + Offset;
						auto& Block = BlocksTypeFull.At(Pos);
						if (Block.IsAir())
							CountAir++;
						if (Block.IsNgen())
							CountNgen++;
					}

					EBlockTreeType Type = EBlockTreeType::Voxel;

					if (CountAir == 8)
						Type = EBlockTreeType::Empty;
					if (CountNgen == 8)
						Type = EBlockTreeType::Full;

					TreeInfo[Level].At({ x, y, z }) = Type;
				}
	}

	{
		PROFILE_SCOPE("FVoxChunk::Init 4")
		for (int Level = TreeLevels - 3; Level >= 0; Level--)
		{
			int Size = 1 << Level;

			for (int x = 0; x < Size; x++)
				for (int y = 0; y < Size; y++)
					for (int z = 0; z < Size; z++)
					{
						FIntVector PosBase(x * 2, y * 2, z * 2);

						int CountAir = 0;
						int CountNgen = 0;

						for (auto& Offset : ChildrenOffsets)
						{
							FIntVector Pos = PosBase + Offset;
							auto& Block = TreeInfo[Level + 1].At(Pos);
							if (Block == EBlockTreeType::Empty)
								CountAir++;
							if (Block == EBlockTreeType::Full)
								CountNgen++;
						}

						EBlockTreeType Type = EBlockTreeType::Voxel;

						//if (CountAir == 8)
						//	Type = EBlockTreeType::Empty;
						//if (CountNgen == 8)
						//	Type = EBlockTreeType::Full;

						TreeInfo[Level].At({ x, y, z }) = Type;
					}
		}
	}

	if (0)
	{
		int Level = TreeLevels - 2;
		int Size = 1 << Level;

		for (int x = 0; x < Size; x++)
			for (int y = 0; y < Size; y++)
				for (int z = 0; z < Size; z++)
				{
					if (TreeInfo[Level].At({ x, y, z }) != EBlockTreeType::Voxel)
						continue;

					DrawDebugBox(
						World,
						FVector((x + 0.5) * 2, (y + 0.5) * 2, (z + 0.5) * 2),
						FVector(1, 1, 1),
						FColor::White,
						false,
						500);
				}
	}

	if (0)
	{
		int Level = 3;
		int Size = 1 << Level;

		for (int x = 0; x < Size; x++)
			for (int y = 0; y < Size; y++)
				for (int z = 0; z < Size; z++)
				{
					if (TreeInfo[Level].At({ x, y, z }) != EBlockTreeType::Voxel)
						continue;

					DrawDebugBox(
						World,
						FVector((x + 0.5) * 2, (y + 0.5) * 2, (z + 0.5) * 2),
						FVector(1, 1, 1),
						FColor::White,
						false,
						500);
				}
	}

	if (0)
	{
		for (int x = 1; x < ChunkSize + 1; x++)
			for (int y = 1; y < ChunkSize + 1; y++)
				for (int z = 1; z < ChunkSize + 1; z++)
				{
					if (BlocksTypeFull.At({ x, y, z }) == BlockTypeAir)
						continue;
					if (BlocksTypeFull.At({ x, y, z }) == BlockTypeNgen)
						continue;

					DrawDebugPoint(
						World,
						FVector((x + 0.5), (y + 0.5), (z + 0.5)),
						2,
						FColor::White,
						false,
						500);
				}
	}

	struct FQueueItem
	{
		FIntVector Pos = FIntVector::ZeroValue;
		int Idx = 0;
		int Level = 0;
	};

	TArray<FQueueItem> Queue;
	Queue.Add({});
	Nodes.Add({});

	{
		PROFILE_SCOPE("FVoxChunk::Init 5")
		for (int i = 0; i < ChunkSize * ChunkSize * ChunkSize * 2; i++)
		{
			if (i >= Queue.Num())
				break;

			auto CurrentNodeInfo = Queue[i];

			if (CurrentNodeInfo.Level < TreeLevels - 2)
			{
				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
						for (int z = 0; z < 2; z++)
						{
							auto& Node = Nodes[CurrentNodeInfo.Idx];
							FIntVector PosChild(CurrentNodeInfo.Pos.X * 2 + x, CurrentNodeInfo.Pos.Y * 2 + y, CurrentNodeInfo.Pos.Z * 2 + z);

							auto& ChildInfo = TreeInfo[CurrentNodeInfo.Level + 1].At(PosChild);
							switch (ChildInfo)
							{
								case EBlockTreeType::Empty:
									Node.Children[x][y][z] = FChunkNodeRef::CreateLeaf(BlockTypeAir);
									break;
								case EBlockTreeType::Full:
									Node.Children[x][y][z] = FChunkNodeRef::CreateLeaf(BlockTypeNgen);
									break;
								case EBlockTreeType::Voxel:
								{
									int NewNodeIdx = Nodes.Num();
									Node.Children[x][y][z] = FChunkNodeRef::CreateNode(NewNodeIdx);
									Nodes.Add({});
									Queue.Add({ PosChild, NewNodeIdx, CurrentNodeInfo.Level + 1 });
								}
								break;
								default:;
							}
						}
			}
			else
			{
				// Leafs voxels

				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
						for (int z = 0; z < 2; z++)
						{
							auto& Node = Nodes[CurrentNodeInfo.Idx];

							FIntVector PosChild(CurrentNodeInfo.Pos.X * 2 + x + 1, CurrentNodeInfo.Pos.Y * 2 + y + 1, CurrentNodeInfo.Pos.Z * 2 + z + 1);

							auto& Block = BlocksTypeFull.At(PosChild);

							if (Block == BlockTypeAir)
							{
								Node.Children[x][y][z] = FChunkNodeRef::CreateLeaf(BlockTypeAir);
							}
							else if (Block == BlockTypeNgen)
							{
								Node.Children[x][y][z] = FChunkNodeRef::CreateLeaf(BlockTypeNgen);
							}
							else
							{
								Node.Children[x][y][z] = FChunkNodeRef::CreateLeaf(Block);
							}
						}
			}
		}
	}

	if (0)
	{
		TFunction<void(FIntVector Pos, int Level, int Idx)> Traverser = [&](FIntVector Pos, int Level, int Idx) {
			auto& CurrentNode = Nodes[Idx];

			for (int x = 0; x < 2; x++)
				for (int y = 0; y < 2; y++)
					for (int z = 0; z < 2; z++)
					{
						auto& ChildInfoData = CurrentNode.Children[x][y][z];
						FIntVector PosChild(Pos.X * 2 + x, Pos.Y * 2 + y, Pos.Z * 2 + z);

						auto ChildInfo = ChildInfoData.GetInfo();
						if (ChildInfo.Key == ERefType::Node)
						{

							Traverser(PosChild, Level + 1, ChildInfo.Value);
						}
						if (ChildInfo.Key == ERefType::Leaf)
						{
							FBlockType Block = { ChildInfo.Value };
							if (Block != BlockTypeAir && Block != BlockTypeNgen)
								DrawDebugPoint(
									World,
									FVector((PosChild.X + 0.5), (PosChild.Y + 0.5), (PosChild.Z + 0.5)),
									2,
									FColor::White,
									false,
									500);
						}
					}
		};
		Traverser(FIntVector::ZeroValue, 0, 0);
	}

	Nodes.Shrink();
}

void FVoxChunk::InitAdvanced(
	TFunction<FBlockType(FIntVector const& Pos)> const& GenFunc,
	TFunction<TOptional<FBlockType>(FIntVector const& Pos, int Level)> const& GenCubeInfo)
{
	PROFILE_FUNCTION()

	TArray<FIntVector> NeighborsOffsets = {
		{ 1, 0, 0 },
		{ -1, 0, 0 },
		{ 0, 1, 0 },
		{ 0, -1, 0 },
		{ 0, 0, 1 },
		{ 0, 0, -1 }
	};

	{
		PROFILE_SCOPE("FVoxChunk::InitAdvanced sides init")
		// Sides init

		for (int SideIdx = 0; SideIdx < 6; SideIdx++)
		{
			EAxis3d Axis1 = EAxis3d(SideIdx % 3);
			EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
			EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
			bool bPositive = SideIdx >= 3;

			FIntVector BasePos = FIntVector::ZeroValue;
			if (bPositive)
				BasePos[(int)Axis1] = ChunkSize;
			else
				BasePos[(int)Axis1] = -1;

			FIntVector VecAxis2 = AxisVec[(int)Axis2];
			FIntVector VecAxis3 = AxisVec[(int)Axis3];

			auto& SideInfo = Sides[SideIdx];

			TFunction<void(FIntVector2 Pos, int Level, int NodeIdx)> Traverser =
				[&](FIntVector2 Pos, int Level, int NodeIdx) {
					for (int x = 0; x < 2; x++)
						for (int y = 0; y < 2; y++)
						{
							FIntVector2 PosChild(Pos.X * 2 + x, Pos.Y * 2 + y);
							if (Level == TreeLevels - 2)
							{
								auto Block = GenFunc(BasePos + VecAxis2 * PosChild.X + VecAxis3 * PosChild.Y);

								SideInfo.Nodes[NodeIdx].Children[x][y] = FChunkNodeRef::CreateLeaf(Block);
							}
							else
							{
								int NewNodeIdx = SideInfo.Nodes.Num();
								SideInfo.Nodes.Add({});
								SideInfo.Nodes[NodeIdx].Children[x][y] = FChunkNodeRef::CreateNode(NewNodeIdx);
								Traverser(PosChild, Level + 1, NewNodeIdx);
							}
						}
				};

			SideInfo.Nodes.Add({});
			Traverser(FIntVector2(0, 0), 0, 0);

			PackSideData(SideInfo);

			/*
			for (int AxisIdx2 = 0; AxisIdx2 < ChunkSize; AxisIdx2++)
				for (int AxisIdx3 = 0; AxisIdx3 < ChunkSize; AxisIdx3++)
				{
					auto SamplePos = BasePos + VecAxis2 * AxisIdx2 + VecAxis3 * AxisIdx3;
				}
			 */
		}
	}

	Nodes.Reset();

	// if (GenCubeInfo({ 0, 0, 0 }, 0) == BlockTypeAir)
	// {
	// 	return;
	// }

	struct FQueueItem
	{
		FIntVector Pos = FIntVector::ZeroValue;
		int Idx = 0;
		int Level = 0;
	};

	{
		PROFILE_SCOPE("FVoxChunk::InitAdvanced Gen basic info")

		TArray<FQueueItem> Queue;
		Queue.Add({});
		Nodes.Add({});

		for (int QueueIdx = 0; QueueIdx < ChunkSize * ChunkSize * ChunkSize * 2; QueueIdx++)
		{
			if (QueueIdx >= Queue.Num())
				break;

			auto CurrentNodeInfo = Queue[QueueIdx];

			if (CurrentNodeInfo.Level < TreeLevels - 2)
			{
				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
						for (int z = 0; z < 2; z++)
						{
							auto& Node = Nodes[CurrentNodeInfo.Idx];
							FIntVector PosChildCube(CurrentNodeInfo.Pos.X * 2 + x, CurrentNodeInfo.Pos.Y * 2 + y, CurrentNodeInfo.Pos.Z * 2 + z);

							TOptional<FBlockType> BlockTreeType = GenCubeInfo(PosChildCube, CurrentNodeInfo.Level + 1);

							if (BlockTreeType)
							{
								Node.Children[x][y][z] = FChunkNodeRef::CreateLeaf(*BlockTreeType);
							}
							else
							{
								int NewNodeIdx = Nodes.Num();
								Node.Children[x][y][z] = FChunkNodeRef::CreateNode(NewNodeIdx);
								Nodes.Add({});
								Queue.Add({ PosChildCube, NewNodeIdx, CurrentNodeInfo.Level + 1 });
							}
						}
			}
			else
			{
				// Leafs voxels

				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
						for (int z = 0; z < 2; z++)
						{
							auto& Node = Nodes[CurrentNodeInfo.Idx];

							FIntVector PosChild(CurrentNodeInfo.Pos.X * 2 + x, CurrentNodeInfo.Pos.Y * 2 + y, CurrentNodeInfo.Pos.Z * 2 + z);

							auto Block = GenFunc(PosChild);

							Node.Children[x][y][z] = FChunkNodeRef::CreateLeaf(Block);
						}
			}
		}
	}

	// DebugRenderSides();

	PackChunkData();
}

void FVoxChunk::PackSidesData()
{
	for (auto& SideData : Sides)
	{
		PackSideData(SideData);
	}
	// SideInfos
}

void FVoxChunk::PackSideData(FVoxSide& SideInfo)
{
	if (SideInfo.Nodes.Num() == 0)
	{
		return;
	}

	{
		PROFILE_SCOPE("FVoxChunk::PackSideData Pack voxels")

		TFunction<TOptional<FBlockType>(FIntVector2 Pos, int Level, int Idx)> Traverser =
			[&](FIntVector2 Pos, int Level, int Idx) -> TOptional<FBlockType> {
			bool Res = false;

			auto& CurrentNode = SideInfo.Nodes[Idx];

			TOptional<FBlockType> BlockChildType;
			bool bTypesDiff = false;

			for (int x = 0; x < 2; x++)
				for (int y = 0; y < 2; y++)
				{
					auto& ChildInfoData = CurrentNode.Children[x][y];
					FIntVector2 PosChild(Pos.X * 2 + x, Pos.Y * 2 + y);

					auto ChildInfo = ChildInfoData.GetInfo();
					if (ChildInfo.Key == ERefType::Node)
					{
						auto ChildType = Traverser(PosChild, Level + 1, ChildInfo.Value);
						if (ChildType)
						{
							if (BlockChildType && BlockChildType != ChildType)
							{
								bTypesDiff = true;
							}
							else
							{
								BlockChildType = ChildType;
							}
							ChildInfoData = FChunkNodeRef::CreateLeaf(*ChildType);
						}
						else
						{
							bTypesDiff = true;
						}
					}
					if (ChildInfo.Key == ERefType::Leaf)
					{
						FBlockType Block = { ChildInfo.Value };
						if (BlockChildType && BlockChildType != Block)
						{
							bTypesDiff = true;
						}
						BlockChildType = Block;
					}
				}

			if (bTypesDiff)
			{
				return {};
			}
			if (!BlockChildType)
			{
				LOG_ERROR();
			}
			return BlockChildType;
		};

		auto FullBlockType = Traverser(FIntVector2(0, 0), 0, 0);

		if (FullBlockType)
		{
			SideInfo.Block = FullBlockType;
			SideInfo.Nodes.Empty();
			return;
		}
	}

	{
		PROFILE_SCOPE("FVoxChunk::PackSideData Remove unused nodes")

		TArray<FVoxChunkSideNode> NodesNew;
		NodesNew.Add({});

		TFunction<void(int IdxNew, int IdxOld, int Level)>
			Traverser = [&](int IdxNew, int IdxOld, int Level) {
				auto const& CurrentNodeOld = SideInfo.Nodes[IdxOld];

				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
					{
						auto& ChildInfoDataOld = CurrentNodeOld.Children[x][y];
						auto RefInfo = ChildInfoDataOld.GetInfo();
						if (RefInfo.Key == ERefType::Leaf)
						{
							NodesNew[IdxNew].Children[x][y] = FChunkNodeRef::CreateLeaf(RefInfo.Value);
						}
						else
						{
							int NewNodeIdx = NodesNew.Num();
							NodesNew.Add({});
							NodesNew[IdxNew].Children[x][y] = FChunkNodeRef::CreateNode(NewNodeIdx);
						}
					}

				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
					{
						auto& ChildInfoDataOld = CurrentNodeOld.Children[x][y];
						auto InfoOld = ChildInfoDataOld.GetInfo();
						if (InfoOld.Key == ERefType::Node)
						{
							auto InfoNew = NodesNew[IdxNew].Children[x][y].GetInfo();
							if (InfoNew.Key != ERefType::Node)
							{
								LOG_ERROR();
							}
							Traverser(InfoNew.Value, InfoOld.Value, Level + 1);
						}
					}
			};
		Traverser(0, 0, 0);

		// DebugRenderTreeLevel(NodesNew);
		SideInfo.Nodes = MoveTemp(NodesNew);
	}

	SideInfo.Nodes.Shrink();

	// LOG("ChunkBytes:" + FString::FromInt(Nodes.Num() * sizeof(Nodes[0])));
}

void FVoxChunk::PackChunkData()
{
	if (Nodes.Num() == 0)
	{
		return;
	}
	{
		PROFILE_SCOPE("FVoxChunk::InitAdvanced Pack ngen voxels")

		TFunction<TOptional<FBlockType>(FIntVector Pos, int Level, int Idx)> Traverser =
			[&](FIntVector Pos, int Level, int Idx) -> TOptional<FBlockType> {
			bool Res = false;

			auto& CurrentNode = Nodes[Idx];

			TOptional<FBlockType> BlockChildType;
			bool bTypesDiff = false;

			for (int x = 0; x < 2; x++)
				for (int y = 0; y < 2; y++)
					for (int z = 0; z < 2; z++)
					{
						auto& ChildInfoData = CurrentNode.Children[x][y][z];
						FIntVector PosChild(Pos.X * 2 + x, Pos.Y * 2 + y, Pos.Z * 2 + z);

						auto ChildInfo = ChildInfoData.GetInfo();
						if (ChildInfo.Key == ERefType::Node)
						{
							auto ChildType = Traverser(PosChild, Level + 1, ChildInfo.Value);
							if (ChildType)
							{
								if (BlockChildType && BlockChildType != ChildType)
								{
									bTypesDiff = true;
								}
								else
								{
									BlockChildType = ChildType;
								}
								ChildInfoData = FChunkNodeRef::CreateLeaf(*ChildType);
							}
							else
							{
								bTypesDiff = true;
							}
						}
						if (ChildInfo.Key == ERefType::Leaf)
						{
							FBlockType Block = { ChildInfo.Value };
							if (BlockChildType && BlockChildType != Block)
							{
								bTypesDiff = true;
							}
							BlockChildType = Block;
						}
					}

			if (bTypesDiff)
			{
				return {};
			}
			if (!BlockChildType)
			{
				LOG_ERROR();
			}
			return BlockChildType;
		};

		auto FullBlockType = Traverser(FIntVector::ZeroValue, 0, 0);

		if (FullBlockType)
		{
			ChunkBlock = FullBlockType;
			Nodes.Empty();
			return;
		}
	}

	{
		PROFILE_SCOPE("FVoxChunk::InitAdvanced Pack nodes")

		TArray<FVoxChunkNode> NodesNew;
		NodesNew.Add({});

		TFunction<void(int IdxNew, int IdxOld, int Level)>
			Traverser = [&](int IdxNew, int IdxOld, int Level) {
				auto const& CurrentNodeOld = Nodes[IdxOld];

				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
						for (int z = 0; z < 2; z++)
						{
							auto& ChildInfoDataOld = CurrentNodeOld.Children[x][y][z];
							auto RefInfo = ChildInfoDataOld.GetInfo();
							if (RefInfo.Key == ERefType::Leaf)
							{
								NodesNew[IdxNew].Children[x][y][z] = FChunkNodeRef::CreateLeaf(RefInfo.Value);
							}
							else
							{
								int NewNodeIdx = NodesNew.Num();
								NodesNew.Add({});
								NodesNew[IdxNew].Children[x][y][z] = FChunkNodeRef::CreateNode(NewNodeIdx);
							}
						}

				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
						for (int z = 0; z < 2; z++)
						{
							auto& ChildInfoDataOld = CurrentNodeOld.Children[x][y][z];
							auto InfoOld = ChildInfoDataOld.GetInfo();
							if (InfoOld.Key == ERefType::Node)
							{
								auto InfoNew = NodesNew[IdxNew].Children[x][y][z].GetInfo();
								if (InfoNew.Key != ERefType::Node)
								{
									LOG_ERROR();
								}
								Traverser(InfoNew.Value, InfoOld.Value, Level + 1);
							}
						}
			};
		Traverser(0, 0, 0);

		// DebugRenderTreeLevel(NodesNew);
		Nodes = MoveTemp(NodesNew);
	}

	Nodes.Shrink();

	// LOG("ChunkBytes:" + FString::FromInt(Nodes.Num() * sizeof(Nodes[0])));
}

FBlockType FVoxChunk::At(FIntVector const& Pos)
{
	if (Nodes.Num() == 0)
		return BlockTypeAir;

	int CurrNodeIdx = 0;

	for (int Level = 0; Level < TreeLevels - 1; Level++)
	{
		auto& Node = Nodes[CurrNodeIdx];

		uint32 BitMask = uint32(1) << (TreeLevels - 2 - Level);

		int x = (Pos.X & BitMask) ? 1 : 0;
		int y = (Pos.Y & BitMask) ? 1 : 0;
		int z = (Pos.Z & BitMask) ? 1 : 0;

		auto Info = Node.Children[x][y][z].GetInfo();
		if (Info.Key == ERefType::Node)
		{
			CurrNodeIdx = Info.Value;
		}
		else
		{
			return FBlockType(Info.Value);
		}
	}

	LOG_ERROR();

	return BlockTypeAir;
}

FChunkNodeRef* FVoxChunk::AtMut(FIntVector const& Pos)
{
	if (Nodes.Num() == 0)
		return nullptr;

	int CurrNodeIdx = 0;

	for (int Level = 0; Level < TreeLevels - 1; Level++)
	{
		auto& Node = Nodes[CurrNodeIdx];

		uint32 BitMask = uint32(1) << (TreeLevels - 2 - Level);

		int x = (Pos.X & BitMask) ? 1 : 0;
		int y = (Pos.Y & BitMask) ? 1 : 0;
		int z = (Pos.Z & BitMask) ? 1 : 0;

		auto Info = Node.Children[x][y][z].GetInfo();
		if (Info.Key == ERefType::Node)
		{
			CurrNodeIdx = Info.Value;
		}
		else
		{
			if (Level == TreeLevels - 2)
			{
				return &Node.Children[x][y][z];
			}
		}
	}

	LOG_ERROR();

	return nullptr;
}

FVoxChunk::FLeafInfo FVoxChunk::AtAdv(FIntVector const& PosSample)
{
	bool bInX = PosSample.X >= 0 && PosSample.X < ChunkSize;
	bool bInY = PosSample.Y >= 0 && PosSample.Y < ChunkSize;
	bool bInZ = PosSample.Z >= 0 && PosSample.Z < ChunkSize;
	if (bInX && bInY && bInZ)
	{
		return AtAdvChunk(PosSample);
	}

	if (PosSample.X == -1 && bInY && bInZ)
	{
		return AtAdvSide(ESideBlock::Xm, PosSample);
	}
	if (PosSample.X == ChunkSize && bInY && bInZ)
	{
		return AtAdvSide(ESideBlock::Xp, PosSample);
	}
	if (bInX && PosSample.Y == -1 && bInZ)
	{
		return AtAdvSide(ESideBlock::Ym, PosSample);
	}
	if (bInX && PosSample.Y == ChunkSize && bInZ)
	{
		return AtAdvSide(ESideBlock::Yp, PosSample);
	}
	if (bInX && bInY && PosSample.Z == -1)
	{
		return AtAdvSide(ESideBlock::Zm, PosSample);
	}
	if (bInX && bInY && PosSample.Z == ChunkSize)
	{
		return AtAdvSide(ESideBlock::Zp, PosSample);
	}

	LOG_ERROR();
	return {};
}

FVoxChunk::FLeafInfo FVoxChunk::AtAdvChunk(FIntVector const& PosSample)
{
	FLeafInfo Res;

	if (Nodes.Num() == 0)
	{
		Res.Pos = { 0, 0, 0 };
		Res.Block = *ChunkBlock;
		Res.Level = 0;
		return Res;
	}

	FIntVector Pos = FIntVector::ZeroValue;

	int CurrNodeIdx = 0;

	for (int Level = 0; Level < TreeLevels - 1; Level++)
	{
		auto& Node = Nodes[CurrNodeIdx];

		uint32 BitMask = uint32(1) << (TreeLevels - 2 - Level);

		int x = (PosSample.X & BitMask) ? 1 : 0;
		int y = (PosSample.Y & BitMask) ? 1 : 0;
		int z = (PosSample.Z & BitMask) ? 1 : 0;

		FIntVector PosChild(Pos.X * 2 + x, Pos.Y * 2 + y, Pos.Z * 2 + z);
		Pos = PosChild;

		auto Info = Node.Children[x][y][z].GetInfo();
		if (Info.Key == ERefType::Node)
		{
			CurrNodeIdx = Info.Value;
		}
		else
		{
			Res.Pos = Pos;
			Res.Block = FBlockType{ Info.Value };
			Res.Level = Level + 1;
			return Res;
		}
	}
	LOG_ERROR();
	return Res;
}

FVoxChunk::FLeafInfo FVoxChunk::AtAdvSide(ESideBlock Side, FIntVector const& PosSample)
{
	EAxis3d Axis1 = EAxis3d((int)Side % 3);
	EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
	EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
	bool bPositive = (int)Side >= 3;

	FIntVector BasePos = FIntVector::ZeroValue;
	if (bPositive)
		BasePos[(int)Axis1] = ChunkSize;
	else
		BasePos[(int)Axis1] = -1;

	FIntVector PosInSide3 = PosSample - BasePos;
	FIntVector2 PosInSide(PosInSide3[(int)Axis2], PosInSide3[(int)Axis3]);

	auto SampleSideLeaf = Sides[(int)Side].AtAdv(PosInSide);

	FLeafInfo Res;
	Res.Pos = { -1, -1, -1 };
	Res.Block = SampleSideLeaf.Block;
	Res.Level = SampleSideLeaf.Level;

	return Res;
}

bool FVoxChunk::IsPosInChunk(FIntVector const& PosSample)
{
	bool bInX = PosSample.X >= 0 && PosSample.X < ChunkSize;
	bool bInY = PosSample.Y >= 0 && PosSample.Y < ChunkSize;
	bool bInZ = PosSample.Z >= 0 && PosSample.Z < ChunkSize;

	return bInX && bInY && bInZ;
}

TArray<FVoxChunk::FLeafInfo> FVoxChunk::GetAllVoxels()
{
	PROFILE_FUNCTION()
	TArray<FLeafInfo> Res;

	if (Nodes.Num() == 0)
	{
		FLeafInfo Info;
		Info.Block = *ChunkBlock;
		Info.Level = 0;
		Info.Pos = { 0, 0, 0 };
		Res.Add(Info);
		return Res;
	}

	TFunction<void(FIntVector Pos, int Level, int Idx)> Traverser = [&](FIntVector Pos, int Level, int Idx) {
		auto& CurrentNode = Nodes[Idx];

		for (int x = 0; x < 2; x++)
			for (int y = 0; y < 2; y++)
				for (int z = 0; z < 2; z++)
				{
					auto& ChildInfoData = CurrentNode.Children[x][y][z];
					FIntVector PosChild(Pos.X * 2 + x, Pos.Y * 2 + y, Pos.Z * 2 + z);

					auto ChildInfo = ChildInfoData.GetInfo();
					if (ChildInfo.Key == ERefType::Node)
					{
						Traverser(PosChild, Level + 1, ChildInfo.Value);
					}
					if (ChildInfo.Key == ERefType::Leaf)
					{
						FBlockType Block = { ChildInfo.Value };
						// if (Block != BlockTypeAir && Block != BlockTypeNgen)
						{
							if (Level + 1 >= FVoxChunk::TreeLevels)
							{
								LOG_ERROR("Voxel on incorrect level:" + FString::FromInt(Level) + " Max:" + FString::FromInt(FVoxChunk::TreeLevels - 1));
							}
							FLeafInfo Info = { .Pos = PosChild, .Block = Block, .Level = Level + 1 };

							Res.Add(Info);
						}
					}
				}
	};
	Traverser(FIntVector::ZeroValue, 0, 0);

	return Res;
}

void FVoxChunk::DebugRenderVoxels(int MaxDrawLevel)
{
	FWorldContext* WorldC = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
	UWorld* World = WorldC ? WorldC->World() : nullptr;

	TFunction<void(FIntVector Pos, int Level, int Idx)> Traverser = [&](FIntVector Pos, int Level, int Idx) {
		auto& CurrentNode = Nodes[Idx];

		if (Level >= MaxDrawLevel - 1)
		{
			DrawDebugBox(
				World,
				FVector((Pos.X + 0.5), (Pos.Y + 0.5), (Pos.Z + 0.5)) * LevelCubeSize(Level),
				FVector(LevelCubeSize(Level) / 2),
				FColor::Black,
				false,
				500);
			return;
		}

		for (int x = 0; x < 2; x++)
			for (int y = 0; y < 2; y++)
				for (int z = 0; z < 2; z++)
				{
					auto& ChildInfoData = CurrentNode.Children[x][y][z];
					FIntVector PosChild(Pos.X * 2 + x, Pos.Y * 2 + y, Pos.Z * 2 + z);

					auto ChildInfo = ChildInfoData.GetInfo();
					if (ChildInfo.Key == ERefType::Node)
					{
						Traverser(PosChild, Level + 1, ChildInfo.Value);
					}
					if (ChildInfo.Key == ERefType::Leaf)
					{
						FBlockType Block = { ChildInfo.Value };
						if (Block != BlockTypeAir)
						{
							// DrawDebugPoint(
							// 	World,
							// 	FVector((Pos.X + 0.5), (Pos.Y + 0.5), (Pos.Z + 0.5)) * LevelCubeSize(Level),
							// 	2,
							// 	FColor::White,
							// 	false,
							// 	500);
							DrawDebugBox(
								World,
								FVector((PosChild.X + 0.5), (PosChild.Y + 0.5), (PosChild.Z + 0.5)) * LevelCubeSize(Level + 1),
								FVector(LevelCubeSize(Level + 1) / 2),
								FColor::White,
								false,
								500);
						}
					}
				}
	};
	Traverser(FIntVector::ZeroValue, 0, 0);
}

void FVoxChunk::DebugRenderSides()
{
	FWorldContext* WorldC = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
	UWorld* World = WorldC ? WorldC->World() : nullptr;

	for (int SideIdx = 0; SideIdx < 6; SideIdx++)
	{
		// if (ESideBlock(SideIdx) != ESideBlock::Zp)
		// {
		// 	continue;
		// }
		EAxis3d Axis1 = EAxis3d(SideIdx % 3);
		EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
		EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
		bool bPositive = SideIdx >= 3;

		FIntVector BasePos = FIntVector::ZeroValue;
		if (bPositive)
			BasePos[(int)Axis1] = ChunkSize;
		else
			BasePos[(int)Axis1] = -1;

		FIntVector VecAxis1 = AxisVec[(int)Axis1];
		FIntVector VecAxis2 = AxisVec[(int)Axis2];
		FIntVector VecAxis3 = AxisVec[(int)Axis3];

		auto& SideInfo = Sides[SideIdx];

		if (SideInfo.Nodes.Num() == 0)
		{
			FVector PixPosAxis;
			PixPosAxis.X = 0.5;
			PixPosAxis.Y = (0 + 0.5) * LevelCubeSize(0);
			PixPosAxis.Z = (0 + 0.5) * LevelCubeSize(0);

			FVector PixPos = FVector(BasePos) + FVector(VecAxis1) * PixPosAxis.X + FVector(VecAxis2) * PixPosAxis.Y + FVector(VecAxis3) * PixPosAxis.Z;

			FVector BoxSz = FVector(VecAxis1) * 0.5 + FVector(VecAxis2) * LevelCubeSize(0) / 2.0 + FVector(VecAxis3) * LevelCubeSize(0) / 2.0;
			BoxSz.X -= 0.05;
			BoxSz.Y -= 0.05;
			BoxSz.Z -= 0.05;
			// DrawDebugPoint(
			// 	World,
			// 	FVector(PixPos.X, PixPos.Y, PixPos.Z),
			// 	2,
			// 	FColor::White,
			// 	false,
			// 	500);
			DrawDebugBox(
				World,
				FVector(PixPos.X, PixPos.Y, PixPos.Z),
				BoxSz,
				GBlocksTypeInfo[SideInfo.Block->BlockType].Color,
				false,
				500);

			continue;
		}

		TFunction<void(FIntVector2 Pos, int Level, int NodeIdx)> Traverser =
			[&](FIntVector2 Pos, int Level, int NodeIdx) {
				for (int x = 0; x < 2; x++)
					for (int y = 0; y < 2; y++)
					{
						FIntVector2 PosChild(Pos.X * 2 + x, Pos.Y * 2 + y);
						auto ChildInfo = SideInfo.Nodes[NodeIdx].Children[x][y].GetInfo();

						if (ChildInfo.Key == ERefType::Node)
						{
							Traverser(PosChild, Level + 1, ChildInfo.Value);
						}
						else
						{
							FVector PixPosAxis;
							PixPosAxis.X = 0.5;
							PixPosAxis.Y = (PosChild.X + 0.5) * LevelCubeSize(Level + 1);
							PixPosAxis.Z = (PosChild.Y + 0.5) * LevelCubeSize(Level + 1);

							FVector PixPos = FVector(BasePos) + FVector(VecAxis1) * PixPosAxis.X + FVector(VecAxis2) * PixPosAxis.Y + FVector(VecAxis3) * PixPosAxis.Z;

							FVector BoxSz = FVector(VecAxis1) * 0.5 + FVector(VecAxis2) * LevelCubeSize(Level + 1) / 2.0 + FVector(VecAxis3) * LevelCubeSize(Level + 1) / 2.0;
							BoxSz.X -= 0.05;
							BoxSz.Y -= 0.05;
							BoxSz.Z -= 0.05;
							// DrawDebugPoint(
							// 	World,
							// 	FVector(PixPos.X, PixPos.Y, PixPos.Z),
							// 	2,
							// 	FColor::White,
							// 	false,
							// 	500);
							DrawDebugBox(
								World,
								FVector(PixPos.X, PixPos.Y, PixPos.Z),
								BoxSz,
								GBlocksTypeInfo[ChildInfo.Value].Color,
								false,
								500);
						}
					}
			};

		Traverser(FIntVector2(0, 0), 0, 0);
		PackSideData(SideInfo);
	}
}

// void FVoxChunk::DebugRenderVoxels()
// {
// 	FWorldContext* WorldC = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
// 	UWorld* World = WorldC ? WorldC->World() : nullptr;
//
// 	TFunction<void(FIntVector Pos, int Level, int Idx)> Traverser = [&](FIntVector Pos, int Level, int Idx) {
// 		auto& CurrentNode = Nodes[Idx];
//
// 		for (int x = 0; x < 2; x++)
// 			for (int y = 0; y < 2; y++)
// 				for (int z = 0; z < 2; z++)
// 				{
// 					auto& ChildInfoData = CurrentNode.Children[x][y][z];
// 					FIntVector PosChild(Pos.X * 2 + x, Pos.Y * 2 + y, Pos.Z * 2 + z);
//
// 					auto ChildInfo = ChildInfoData.GetInfo();
// 					if (ChildInfo.Key == ERefType::Node)
// 					{
// 						Traverser(PosChild, Level + 1, ChildInfo.Value);
// 					}
// 					if (ChildInfo.Key == ERefType::Leaf)
// 					{
// 						FBlockType Block = { ChildInfo.Value };
// 						if (Block != BlockTypeAir && Block != BlockTypeNgen)
// 							DrawDebugPoint(
// 								World,
// 								FVector((PosChild.X + 0.5), (PosChild.Y + 0.5), (PosChild.Z + 0.5)),
// 								2,
// 								FColor::White,
// 								false,
// 								500);
// 					}
// 				}
// 	};
// 	Traverser(FIntVector::ZeroValue, 0, 0);
// }

void FVoxChunk::DebugRenderVoxels2()
{
	FWorldContext* WorldC = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
	UWorld* World = WorldC ? WorldC->World() : nullptr;

	for (int x = 0; x < ChunkSize; x++)
		for (int y = 0; y < ChunkSize; y++)
			for (int z = 0; z < ChunkSize; z++)
			{
				FBlockType Block = At({ x, y, z });

				if (Block == BlockTypeAir)
					continue;
				if (Block == BlockTypeNgen)
					continue;

				DrawDebugPoint(
					World,
					FVector((x + 0.5), (y + 0.5), (z + 0.5)),
					2,
					FColor::White,
					false,
					500);
			}
}

void FVoxChunk::DebugRenderTreeLevel(TArray<FVoxChunkNode> const& NodesT)
{
	FWorldContext* WorldC = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
	UWorld* World = WorldC ? WorldC->World() : nullptr;

	TFunction<void(FIntVector Pos, int Level, int Idx)> Traverser = [&](FIntVector Pos, int Level, int Idx) {
		auto& CurrentNode = NodesT[Idx];

		if (Level == 5)
		{
			DrawDebugPoint(
				World,
				FVector((Pos.X + 0.5), (Pos.Y + 0.5), (Pos.Z + 0.5)),
				2,
				FColor::White,
				false,
				500);
			return;
		}

		for (int x = 0; x < 2; x++)
			for (int y = 0; y < 2; y++)
				for (int z = 0; z < 2; z++)
				{
					auto& ChildInfoData = CurrentNode.Children[x][y][z];
					FIntVector PosChild(Pos.X * 2 + x, Pos.Y * 2 + y, Pos.Z * 2 + z);

					auto ChildInfo = ChildInfoData.GetInfo();
					if (ChildInfo.Key == ERefType::Node)
					{
						Traverser(PosChild, Level + 1, ChildInfo.Value);
					}
					// if (ChildInfo.Key == ERefType::Leaf)
					// {
					// 	FBlockType Block = { ChildInfo.Value };
					// 	if (Block != BlockTypeAir && Block != BlockTypeNgen)
					// 		DrawDebugPoint(
					// 			World,
					// 			FVector((PosChild.X + 0.5), (PosChild.Y + 0.5), (PosChild.Z + 0.5)),
					// 			2,
					// 			FColor::White,
					// 			false,
					// 			500);
					// }
				}
	};
	Traverser(FIntVector::ZeroValue, 0, 0);
}

enum class ENeedRect
{
	A,
	B
};

TOptional<ENeedRect> GetRectReq(FBlockType A, FBlockType B)
{
	if (!A.IsAir() && B.IsAir())
	{
		return ENeedRect::A;
	}
	if (A.IsAir() && !B.IsAir())
	{
		return ENeedRect::B;
	}
	if (A.IsSolid() && B.IsTranslucent())
	{
		return ENeedRect::A;
	}
	if (A.IsTranslucent() && B.IsSolid())
	{
		return ENeedRect::B;
	}
	return {};
}

void FVoxChunk::GenMeshData()
{
	PROFILE_FUNCTION()

	// TODO try create traverse func
	auto Leafs = GetAllVoxels();

	struct FSideRectInfo
	{
		FIntVector Pos;
		FBlockType Type;
	};

	TArray<TArray<FSideRectInfo>> RectsPerSide;
	RectsPerSide.SetNum(6);

	for (auto& LeafInfo : Leafs)
	{
		FIntVector VoxelPosLastLevel = LeafInfo.Pos * LevelCubeSize(LeafInfo.Level);

		for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
		{
			EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
			EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
			EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
			bool bPositive = SideBlockIdx >= 3;

			FIntVector BlockPosNe = VoxelPosLastLevel;
			if (bPositive)
				BlockPosNe[(int)Axis1] += LevelCubeSize(LeafInfo.Level);
			else
				BlockPosNe[(int)Axis1]--;

			auto LeafInfoNe = AtAdv(BlockPosNe);

			TOptional<ENeedRect> NeedRectOpt;

			if (LeafInfoNe.Level < LeafInfo.Level)
			{
				auto ReqRect = GetRectReq(LeafInfo.Block, LeafInfoNe.Block);
				NeedRectOpt = ReqRect;
				if (NeedRectOpt == ENeedRect::B && !IsPosInChunk(BlockPosNe))
				{
					NeedRectOpt.Reset();
				}
			}
			else if (LeafInfoNe.Level == LeafInfo.Level)
			{
				auto ReqRect = GetRectReq(LeafInfo.Block, LeafInfoNe.Block);
				if (ReqRect && ReqRect == ENeedRect::A)
					NeedRectOpt = ReqRect;
			}

			if (NeedRectOpt)
			{
				auto NeedRect = *NeedRectOpt;
				FIntVector LoopBasePos = VoxelPosLastLevel;
				if (bPositive)
				{
					LoopBasePos[(int)Axis1] += LevelCubeSize(LeafInfo.Level) - 1;
				}
				if (NeedRect == ENeedRect::B)
				{
					if (bPositive)
						LoopBasePos[(int)Axis1] += 1;
					else
						LoopBasePos[(int)Axis1] -= 1;
				}

				FBlockType RectBlockType = NeedRect == ENeedRect::A ? LeafInfo.Block : LeafInfoNe.Block;

				for (int x = 0; x < LevelCubeSize(LeafInfo.Level); x++)
					for (int y = 0; y < LevelCubeSize(LeafInfo.Level); y++)
					{
						FSideRectInfo Info;
						Info.Pos = LoopBasePos + AxisVec[(int)Axis2] * x + AxisVec[(int)Axis3] * y;
						Info.Type = RectBlockType;
						if (NeedRect == ENeedRect::A)
						{
							RectsPerSide[SideBlockIdx].Add(Info);
						}
						else
						{
							if (SideBlockIdx < 3)
								RectsPerSide[SideBlockIdx + 3].Add(Info);
							else
							{
								RectsPerSide[SideBlockIdx - 3].Add(Info);
							}
						}
					}
			}
		}
	}

	// LeafsBySides
	for (int SideIdx = 0; SideIdx < 6; SideIdx++)
	{
		int SideBlockIdx = (SideIdx + 3) % 6;
		EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
		EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
		EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
		bool bPositive = SideBlockIdx >= 3;

		auto& Side = Sides[SideIdx];

		auto SideLeafs = Side.GetAllVoxels();

		FIntVector BasePosition = FIntVector::ZeroValue;
		if (!bPositive) // opposite
			BasePosition[(int)Axis1] += ChunkSize;
		else
			BasePosition[(int)Axis1] -= 1;

		for (auto& Leaf : SideLeafs)
		{
			FIntVector VoxelPosLastLevel = BasePosition + AxisVec[(int)Axis2] * (Leaf.Pos.X * LevelCubeSize(Leaf.Level)) + AxisVec[(int)Axis3] * (Leaf.Pos.Y * LevelCubeSize(Leaf.Level));

			FIntVector BlockPosNe = VoxelPosLastLevel;
			if (bPositive)
				BlockPosNe[(int)Axis1]++;
			else
				BlockPosNe[(int)Axis1]--;

			auto LeafInfoNe = AtAdv(BlockPosNe);

			auto NeedRectOpt = GetRectReq(Leaf.Block, LeafInfoNe.Block);

			if (LeafInfoNe.Level >= Leaf.Level)
				continue;
			if (NeedRectOpt != ENeedRect::B)
				continue;

			if (NeedRectOpt)
			{
				auto NeedRect = *NeedRectOpt;
				FIntVector LoopBasePos = VoxelPosLastLevel;
				if (bPositive)
				{
					LoopBasePos[(int)Axis1] += LevelCubeSize(Leaf.Level) - 1;
				}
				if (NeedRect == ENeedRect::B)
				{
					if (bPositive)
						LoopBasePos[(int)Axis1] += 1;
					else
						LoopBasePos[(int)Axis1] -= 1;
				}

				FBlockType RectBlockType = LeafInfoNe.Block;

				for (int x = 0; x < LevelCubeSize(Leaf.Level); x++)
					for (int y = 0; y < LevelCubeSize(Leaf.Level); y++)
					{
						FSideRectInfo Info;
						Info.Pos = LoopBasePos + AxisVec[(int)Axis2] * x + AxisVec[(int)Axis3] * y;
						Info.Type = RectBlockType;
						if (NeedRect == ENeedRect::A)
						{							
							LOG_ERROR();
							RectsPerSide[SideBlockIdx].Add(Info);
						}
						else
						{
							if (SideBlockIdx < 3)
								RectsPerSide[SideBlockIdx + 3].Add(Info);
							else
							{
								RectsPerSide[SideBlockIdx - 3].Add(Info);
							}
						}
					}
			}
		}
	}

	// {
	// 	FWorldContext* WorldC = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
	// 	UWorld* World = WorldC ? WorldC->World() : nullptr;
	//
	// 	for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
	// 	{
	// 		EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
	// 		EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
	// 		EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
	//
	// 		auto& SideRects = RectsPerSide[SideBlockIdx];
	//
	// 		bool bPositive = SideBlockIdx >= 3;
	//
	// 		for (auto& SideInfo : SideRects)
	// 		{
	// 			FVector PlaneCentre = FVector(SideInfo.Pos) + FVector(0.5, 0.5, 0.5);
	//
	// 			if (!bPositive)
	// 				PlaneCentre[(int)Axis1] -= 0.5;
	// 			else
	// 				PlaneCentre[(int)Axis1] += 0.5;
	//
	// 			FVector PlaneNormal = FVector::ZeroVector;
	// 			PlaneNormal[(int)Axis1] += 1 * bPositive;
	//
	// 			FVector BoxSz(0.5 - 0.05);
	// 			BoxSz[(int)Axis1] = 0;
	// 			DrawDebugBox(
	// 				World,
	// 				PlaneCentre,
	// 				BoxSz,
	// 				FColor::MakeRandomColor(),
	// 				false,
	// 				500);
	// 		}
	// 	}
	// }

	for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
	{
		auto& SideRects = RectsPerSide[SideBlockIdx];
		SideRects.Sort([&](FSideRectInfo const& A, FSideRectInfo const& B) {
			if (A.Pos.X < B.Pos.X)
				return true;
			if (A.Pos.X > B.Pos.X)
				return false;
			if (A.Pos.Y < B.Pos.Y)
				return true;
			if (A.Pos.Y > B.Pos.Y)
				return false;
			if (A.Pos.Z < B.Pos.Z)
				return true;
			if (A.Pos.Z > B.Pos.Z)
				return false;
			LOG_ERROR();
			return false;
		});
	}

	TArray<TMap<FIntVector, FBlockType>> RectsPerSideMap;
	RectsPerSideMap.SetNum(6);
	for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
	{
		auto& SidesMap = RectsPerSideMap[SideBlockIdx];
		auto& SideRects = RectsPerSide[SideBlockIdx];
		for (auto& Rect : SideRects)
		{
			SidesMap.Add(Rect.Pos, Rect.Type);
		}
	}

	struct FGreedySideInfo
	{
		FIntVector Pos;
		FBlockType Type;
		int SizeAxis2 = 1;
		int SizeAxis3 = 1;
	};
	TArray<TArray<FGreedySideInfo>> GreedyRectsPerSide;
	GreedyRectsPerSide.SetNum(6);

	const int MaxMerge = 8;

	for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
	{
		EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
		EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
		EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);

		auto& SidesMap = RectsPerSideMap[SideBlockIdx];
		auto& SideRects = RectsPerSide[SideBlockIdx];
		auto& GreedySides = GreedyRectsPerSide[SideBlockIdx];
		bool bPositive = SideBlockIdx >= 3;

		for (auto& SideInfo : SideRects)
		{
			auto BasePos = SideInfo.Pos;
			if (!SidesMap.Contains(BasePos))
				continue;

			int SizeAxis2 = 0;
			int SizeAxis3 = 0;

			{
				auto ExPos = BasePos;
				while (SidesMap.Contains(ExPos) && SidesMap[ExPos] == SideInfo.Type)
				{
					ExPos[(int)Axis2]++;
					SizeAxis2++;
					if (SizeAxis2 >= MaxMerge)
						break;
				}
			}

			{
				for (int i = 0;; i++)
				{
					bool bAllSame = true;

					auto ExPos = BasePos;
					ExPos[(int)Axis3] += i;
					for (int r = 0; r < SizeAxis2; r++)
					{
						auto BlockPos = ExPos;
						BlockPos[(int)Axis2] += r;

						if (!SidesMap.Contains(BlockPos))
						{
							bAllSame = false;
							break;
						}
						if (SidesMap[BlockPos] != SideInfo.Type)
						{
							bAllSame = false;
							break;
						}
					}

					if (bAllSame)
					{
						SizeAxis3++;
						if (SizeAxis3 >= MaxMerge)
							break;
					}
					else
					{
						break;
					}
				}
			}

			for (int i = 0; i < SizeAxis2; i++)
				for (int r = 0; r < SizeAxis3; r++)
				{
					auto Pos = BasePos;
					Pos[(int)Axis2] += i;
					Pos[(int)Axis3] += r;

					SidesMap.Remove(Pos);
				}

			FGreedySideInfo Info;

			Info.Pos = SideInfo.Pos;
			Info.Type = SideInfo.Type;
			Info.SizeAxis2 = SizeAxis2;
			Info.SizeAxis3 = SizeAxis3;

			GreedySides.Add(Info);
		}
	}

	if (0)
	{
		FWorldContext* WorldC = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
		UWorld* World = WorldC ? WorldC->World() : nullptr;

		for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
		{
			EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
			EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
			EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
			bool bPositive = SideBlockIdx >= 3;

			auto& GreedySides = GreedyRectsPerSide[SideBlockIdx];

			for (auto& SideInfo : GreedySides)
			{
				FVector PlaneCentre = FVector(SideInfo.Pos) + FVector(0.5, 0.5, 0.5);

				if (!bPositive)
					PlaneCentre[(int)Axis1] -= 0.5;
				else
					PlaneCentre[(int)Axis1] += 0.5;

				FVector PlaneNormal = FVector::ZeroVector;
				PlaneNormal[(int)Axis1] += 1 * bPositive;

				DrawDebugPoint(
					World,
					PlaneCentre,
					2,
					FColor::MakeRandomColor(),
					false,
					500);
			}
		}
	}

	// T-junctions search and remove

	TSet<FIntVector> MapPointsMesh;

	for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
	{
		EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
		EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
		EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
		bool bPositive = SideBlockIdx >= 3;

		auto& GreedySides = GreedyRectsPerSide[SideBlockIdx];

		for (auto& SideInfo : GreedySides)
		{
			FIntVector OffsetAxis2 = FIntVector::ZeroValue;
			OffsetAxis2[(int)Axis2] = SideInfo.SizeAxis2;
			FIntVector OffsetAxis3 = FIntVector::ZeroValue;
			OffsetAxis3[(int)Axis3] = SideInfo.SizeAxis3;

			FIntVector P1 = SideInfo.Pos;
			if (bPositive)
				P1[(int)Axis1]++;
			FIntVector P2 = P1 + OffsetAxis2;
			FIntVector P3 = P1 + OffsetAxis2 + OffsetAxis3;
			FIntVector P4 = P1 + OffsetAxis3;

			MapPointsMesh.Add(P1);
			MapPointsMesh.Add(P2);
			MapPointsMesh.Add(P3);
			MapPointsMesh.Add(P4);
		}
	}

	struct FPolygonSideInfo
	{
		TArray<FIntVector> Points;
		FBlockType Type;
		int ZeroOppositePointIdx = 0;
	};
	TArray<TArray<FPolygonSideInfo>> PolygonsPerSide;
	PolygonsPerSide.SetNum(6);

	for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
	{
		EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
		EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
		EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
		bool bPositive = SideBlockIdx >= 3;

		auto& GreedySides = GreedyRectsPerSide[SideBlockIdx];
		auto& Polygons = PolygonsPerSide[SideBlockIdx];

		for (auto& SideInfo : GreedySides)
		{
			FPolygonSideInfo Polygon;
			Polygon.Type = SideInfo.Type;

			auto IterateLine = [&](FIntVector Start, FIntVector Vec, int Len) {
				for (int i = 0; i < Len; i++)
				{
					auto Pos = Start + Vec * i;

					bool bOnEdge = Pos[0] == 0 || Pos[0] == ChunkSize || Pos[1] == 0 || Pos[1] == ChunkSize || Pos[2] == 0 || Pos[2] == ChunkSize;

					if (MapPointsMesh.Contains(Pos) || bOnEdge)
					{
						Polygon.Points.Add(Pos);
					}
				}
			};

			FIntVector OffsetAxis2 = FIntVector::ZeroValue;
			OffsetAxis2[(int)Axis2] = SideInfo.SizeAxis2;
			FIntVector OffsetAxis3 = FIntVector::ZeroValue;
			OffsetAxis3[(int)Axis3] = SideInfo.SizeAxis3;

			FIntVector VecAxis2 = FIntVector::ZeroValue;
			VecAxis2[(int)Axis2]++;
			FIntVector VecAxis3 = FIntVector::ZeroValue;
			VecAxis3[(int)Axis3]++;

			FIntVector P1 = SideInfo.Pos;
			if (bPositive)
				P1[(int)Axis1]++;
			FIntVector P2 = P1 + OffsetAxis2;
			FIntVector P3 = P1 + OffsetAxis2 + OffsetAxis3;
			FIntVector P4 = P1 + OffsetAxis3;

			IterateLine(P1, VecAxis2, SideInfo.SizeAxis2);
			IterateLine(P2, VecAxis3, SideInfo.SizeAxis3);
			Polygon.ZeroOppositePointIdx = Polygon.Points.Num();
			IterateLine(P3, VecAxis2 * -1, SideInfo.SizeAxis2);
			IterateLine(P4, VecAxis3 * -1, SideInfo.SizeAxis3);

			Polygons.Add(Polygon);
		}
	}

	MeshDataSolid = MakeShared<FChunkMeshData>();
	MeshDataWater = MakeShared<FChunkMeshData>();
	MeshDataTranslucent = MakeShared<FChunkMeshData>();

	auto AddPolygon = [&](FChunkMeshData& MeshData, FPolygonSideInfo const& Polygon,
						  int SideBlockIdx) {
		EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
		EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
		EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
		bool bPositive = SideBlockIdx >= 3;

		int StartIdx = MeshData.Vertices.Num();

		auto GetPointUv = [&](FIntVector const& Pos) {
			FVector2d Res(Pos[(int)Axis2] / (double)ChunkSize, Pos[(int)Axis3] / (double)ChunkSize);

			return Res;
		};

		auto Color = FColor(0, 0, 0, Polygon.Type.BlockType);
		FVector Normal = FVector::ZeroVector;
		if (bPositive)
			Normal[(int)Axis1]++;
		else
			Normal[(int)Axis1]--;

		for (auto& Point : Polygon.Points)
		{
			MeshData.Vertices.Add(FVector(Point));
			MeshData.UV.Add(GetPointUv(Point));
			MeshData.Colors.Add(Color);
			MeshData.Normals.Add(Normal);
		}

		auto AddTriangle = [&](int i1, int i2, int i3) {
			MeshData.Triangles.Add(StartIdx + i1);
			if (bPositive)
			{
				MeshData.Triangles.Add(StartIdx + i3);
				MeshData.Triangles.Add(StartIdx + i2);
			}
			else
			{
				MeshData.Triangles.Add(StartIdx + i2);
				MeshData.Triangles.Add(StartIdx + i3);
			}
		};

		int K = Polygon.ZeroOppositePointIdx;
		int N = Polygon.Points.Num();

		AddTriangle(0, 1, N - 1);
		AddTriangle(K, K + 1, K - 1);

		for (int i = K + 1; i < N - 1; i++)
		{
			AddTriangle(1, i, i + 1);
		}
		for (int i = 1; i < K - 1; i++)
		{
			AddTriangle(K + 1, i, i + 1);
		}
	};
	for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
	{
		auto& Polygons = PolygonsPerSide[SideBlockIdx];
		for (auto& Polygon : Polygons)
		{
			if (Polygon.Type.IsSolid())
				AddPolygon(*MeshDataSolid, Polygon, SideBlockIdx);
			else if (Polygon.Type.IsWater())
				AddPolygon(*MeshDataWater, Polygon, SideBlockIdx);
			else if (Polygon.Type.IsTranslucent())
				AddPolygon(*MeshDataTranslucent, Polygon, SideBlockIdx);
			else
			{
				LOG_ERROR();
			}
		}
	}

	if (MeshDataSolid->Vertices.Num() == 0)
		MeshDataSolid.Reset();
	if (MeshDataTranslucent->Vertices.Num() == 0)
		MeshDataTranslucent.Reset();
	if (MeshDataWater->Vertices.Num() == 0)
		MeshDataWater.Reset();

	// bad try: T-junctions one pixel holes
	// auto AddQuad = [&](
	// 				   FVector const& P1,
	// 				   FVector const& P2,
	// 				   FVector const& P3,
	// 				   FVector const& P4,
	// 				   FVector2d const& UV1,
	// 				   FVector2d const& UV2,
	// 				   FVector2d const& UV3,
	// 				   FVector2d const& UV4,
	// 				   int SizeAxis2,
	// 				   int SizeAxis3,
	// 				   FVector const& Normal,
	// 				   FBlockType const& Block) {
	// 	int StartIdx = MeshData.Vertices.Num();
	//
	// 	MeshData.Vertices.Add(P1 * 100);
	// 	MeshData.Vertices.Add(P2 * 100);
	// 	MeshData.Vertices.Add(P3 * 100);
	// 	MeshData.Vertices.Add(P4 * 100);
	//
	// 	MeshData.UV.Add(UV1);
	// 	MeshData.UV.Add(UV2);
	// 	MeshData.UV.Add(UV3);
	// 	MeshData.UV.Add(UV4);
	//
	// 	auto Color = FColor(0, 0, 0, Block.BlockType);
	//
	// 	MeshData.Colors.Add(Color);
	// 	MeshData.Colors.Add(Color);
	// 	MeshData.Colors.Add(Color);
	// 	MeshData.Colors.Add(Color);
	//
	// 	MeshData.Normals.Add(Normal);
	// 	MeshData.Normals.Add(Normal);
	// 	MeshData.Normals.Add(Normal);
	// 	MeshData.Normals.Add(Normal);
	//
	// 	MeshData.Triangles.Add(StartIdx + 0);
	// 	MeshData.Triangles.Add(StartIdx + 1);
	// 	MeshData.Triangles.Add(StartIdx + 2);
	// 	MeshData.Triangles.Add(StartIdx + 0);
	// 	MeshData.Triangles.Add(StartIdx + 2);
	// 	MeshData.Triangles.Add(StartIdx + 3);
	// };
	//
	// for (int SideBlockIdx = 0; SideBlockIdx < 6; SideBlockIdx++)
	// {
	// 	EAxis3d Axis1 = EAxis3d(SideBlockIdx % 3);
	// 	EAxis3d Axis2 = EAxis3d(((int)Axis1 + 1) % 3);
	// 	EAxis3d Axis3 = EAxis3d(((int)Axis1 + 2) % 3);
	//
	// 	auto& GreedySides = GreedyRectsPerSide[SideBlockIdx];
	//
	// 	bool bPositive = SideBlockIdx >= 3;
	// 	for (auto& SideInfo : GreedySides)
	// 	{
	// 		FVector BasePos = FVector(SideInfo.Pos);
	// 		if (bPositive)
	// 			BasePos[(int)Axis1]++;
	//
	// 		FVector OffsetAxis2 = FVector::ZeroVector;
	// 		OffsetAxis2[(int)Axis2] = SideInfo.SizeAxis2;
	// 		FVector OffsetAxis3 = FVector::ZeroVector;
	// 		OffsetAxis3[(int)Axis3] = SideInfo.SizeAxis3;
	//
	// 		FVector P1 = BasePos;
	// 		FVector P2 = BasePos + OffsetAxis2;
	// 		FVector P3 = BasePos + OffsetAxis2 + OffsetAxis3;
	// 		FVector P4 = BasePos + OffsetAxis3;
	//
	// 		if (bPositive)
	// 		{
	// 			Swap(P1, P2);
	// 			Swap(P3, P4);
	// 		}
	//
	// 		FVector2d UV1 = FVector2d{ P1[(int)Axis2], P1[(int)Axis3] } / 256.0;
	// 		FVector2d UV2 = FVector2d{ P2[(int)Axis2], P2[(int)Axis3] } / 256.0;
	// 		FVector2d UV3 = FVector2d{ P3[(int)Axis2], P3[(int)Axis3] } / 256.0;
	// 		FVector2d UV4 = FVector2d{ P4[(int)Axis2], P4[(int)Axis3] } / 256.0;
	//
	// 		FVector Normal = FVector::ZeroVector;
	// 		if (bPositive)
	// 			Normal[(int)Axis1]++;
	// 		else
	// 			Normal[(int)Axis1]--;
	// 		FBlockType Block = SideInfo.Type;
	//
	// 		AddQuad(P1, P2, P3, P4, UV1, UV2, UV3, UV4, SideInfo.SizeAxis2, SideInfo.SizeAxis3, Normal, Block);
	// 	}
	// }
}
int FVoxChunk::LevelCubeSize(int Level)
{
	return ChunkSize >> Level;
}
FVector FVoxChunk::GetSectorCenter(FIntVector const& SectorPos)
{
	return (FVector(SectorPos) + 0.5) * ChunkSize;
}