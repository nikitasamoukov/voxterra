#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogVoxTerra, Log, All);

void LOG_IMPLWarning(const char* File, int Line, const char* Function, FString const& Text);
void LOG_IMPLError(const char* File, int Line, const char* Function, FString const& Text = "UNEXPECTED");

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define LOG(msg) (LOG_IMPLWarning(__FILENAME__, __LINE__, __FUNCTION__, (msg)))
#define LOG_ERROR(...) (LOG_IMPLError(__FILENAME__, __LINE__, __FUNCTION__, __VA_ARGS__))