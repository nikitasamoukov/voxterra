﻿#include "BlockTypeInfo.h"

#include "BlockType.h"

const TArray<FBlockTypeInfo> GBlocksTypeInfo = []() {
	TArray<FBlockTypeInfo> Res;
	Res.SetNum(200);

	Res[BlockTypeWater.BlockType].bIsTranslucent = true;
	
	Res[BlockTypeGlass.BlockType].bIsTranslucent = true;

	//0
	Res[BlockTypeAir.BlockType].Color = FColor::FromHex("FFFFFF");
	//1
	Res[BlockTypeNgen.BlockType].Color = FColor::FromHex("000000");

	//2
	Res[BlockTypeGrass.BlockType].Color = FColor::FromHex("00FF00");
	Res[BlockTypeDirt.BlockType].Color = FColor::FromHex("442200");
	Res[BlockTypeStone.BlockType].Color = FColor::FromHex("888888");
	Res[BlockTypeSand.BlockType].Color = FColor::FromHex("FFDD66");
	Res[BlockTypeSnow.BlockType].Color = FColor::FromHex("EEEEEE");
	
	Res[BlockTypeWater.BlockType].Color = FColor::FromHex("0000CC");

	return Res;
}();