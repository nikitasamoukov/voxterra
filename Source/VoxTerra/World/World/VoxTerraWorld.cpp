﻿#include "VoxTerraWorld.h"

#include "ChunksManager.h"
#include "ProceduralMeshComponent.h"
#include "VoxTerra/World/Generator/ChunkGenerator.h"
#include "VoxTerra/World/Generator/LandBaseCache.h"
#include "VoxTerra/World/Terrain/GameChunk.h"

UTerrainChunkData::UTerrainChunkData()
{
}

AVoxTerraWorld::AVoxTerraWorld()
{
	ChunksManager = MakeShared<FChunksManager>();

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	PrimaryActorTick.bCanEverTick = true;
}

void AVoxTerraWorld::BeginPlay()
{
	PROFILE_FUNCTION()
	Super::BeginPlay();

	/*
	int64 Bytes = 0;
	for (auto& Pair : Chunks)
	{
		int64 ChunkBytes = Pair.Value->Chunk->Nodes.Num() * sizeof(Pair.Value->Chunk->Nodes[0]);
		LOG("Chunk (" + Pair.Key.ToString() + ") Kb:" + FString::SanitizeFloat(ChunkBytes / 1024.0));

		Bytes += ChunkBytes;
	}
	LOG("World kb:" + FString::SanitizeFloat(Bytes / 1024.0));
	*/

	// ChunkData->Chunk->DebugRenderVoxels2();
}

void AVoxTerraWorld::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	/*
	FIntVector SectorPos = { 0, 0, 0 };
	FVector ActorPos = FVector(SectorPos) * FVoxChunk::ChunkSize;

	if (!Chunks.Contains(SectorPos))
	{
		auto Chunk = ChunkTestVox();
		auto ChunkData = NewObject<UTerrainChunkData>(this);
		Chunks.Add(SectorPos, ChunkData);

		ChunkData->Chunk = Chunk;
		AGameChunk* ChunkActor = AGameChunk::Create(GetWorld(), FTransform(ActorPos), this, Chunk);
		ChunkData->ChunkActor = ChunkActor;

		Chunk->GenMeshData();

		ChunkActor->UpdateMesh();
		if (LandMaterialSolid)
			ChunkActor->Mesh->SetMaterial(0, LandMaterialSolid);
		if (LandMaterialTranslucent)
			ChunkActor->Mesh->SetMaterial(1, LandMaterialTranslucent);
		if (LandMaterialWater)
			ChunkActor->Mesh->SetMaterial(2, LandMaterialWater);
	}
*/
	ChunksManager->Update(PlayerPos, DistanceLoad, DistanceUnload);

	auto CurrentChunks = ChunksManager->GetChunks();

	TArray<TPair<FIntVector, TSharedPtr<FVoxChunk>>> SectorsToLoad;

	for (auto& Pair : CurrentChunks)
	{
		if (!Chunks.Contains(Pair.Key))
			SectorsToLoad.Add(Pair);
	}

	for (auto& SectorData : SectorsToLoad)
	{
		auto SectorPos = SectorData.Key;

		FVector ActorPos = FVector(SectorPos) * FVoxChunk::ChunkSize;

		auto ChunkData = NewObject<UTerrainChunkData>(this);
		Chunks.Add(SectorPos, ChunkData);

		// ChunkData->Chunk = ChunkTestSphereGen();
		// ChunkData->Chunk = ChunkTestCubicGen();

		ChunkData->Chunk = SectorData.Value;

		AGameChunk* ChunkActor = AGameChunk::Create(GetWorld(), FTransform(ActorPos), this, ChunkData->Chunk);
		ChunkData->ChunkActor = ChunkActor;
		ChunkActor->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

		ChunkActor->UpdateMesh();
		if (LandMaterialSolid)
			ChunkActor->Mesh->SetMaterial(0, LandMaterialSolid);
		if (LandMaterialTranslucent)
			ChunkActor->Mesh->SetMaterial(1, LandMaterialTranslucent);
		if (LandMaterialWater)
			ChunkActor->Mesh->SetMaterial(2, LandMaterialWater);
	}
}

void AVoxTerraWorld::TestSectorGen()
{
	TSharedPtr<FLandBaseGenerator> Gen = MakeShared<FLandBaseGenerator>(1337);
	FLandBaseSector Sector;
	Sector.Generate(Gen, FIntVector2(TestSectorPos.X, TestSectorPos.Y));

	// AGameChunk* ChunkActor = AGameChunk::Create(GetWorld(), FTransform(), this, nullptr);
	FLandBaseSector::FDebugOutput Info;
	// Info.MeshComponent = ChunkActor->Mesh;
	Sector.GetDebugInfo(Info);

	TestSectorTexture = Info.Texture;
	TestSectorTexture2 = Info.Texture2;
}