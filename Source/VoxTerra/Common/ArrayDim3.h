﻿#pragma once

template <typename T>
class TArrayDim3
{
public:
	explicit TArrayDim3(FIntVector const& SizeNew = { 0, 0, 0 });
	void SetSize(FIntVector const& SizeNew = { 0, 0, 0 });

	auto& Num() const { return ArraySize; }
	auto& RawData() const { return ArrayRawData; }
	auto& At(FIntVector const& Idx) { return ArrayRawData[Idx.X + (Idx.Y + Idx.Z * ArraySize.Y) * ArraySize.X]; }
	auto& At(int X, int Y, int Z) { return At({X, Y, Z}); }

private:
	TArray<T> ArrayRawData;
	FIntVector ArraySize;
};

template <typename T>
TArrayDim3<T>::TArrayDim3(FIntVector const& SizeNew)
{
	SetSize(SizeNew);
}
template <typename T>
void TArrayDim3<T>::SetSize(FIntVector const& SizeNew)
{
	ArraySize = SizeNew;

	ArrayRawData.SetNum(ArraySize.X * ArraySize.Y * ArraySize.Z);
}