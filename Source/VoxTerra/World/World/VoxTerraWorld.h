﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VoxTerraWorld.generated.h"

struct FChunksManager;
struct FVoxChunk;
class UProceduralMeshComponent;
class AGameChunk;

UCLASS()
class VOXTERRA_API UTerrainChunkData : public UObject
{
	GENERATED_BODY()

public:
	UTerrainChunkData();

	TSharedPtr<FVoxChunk> Chunk;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	AGameChunk* ChunkActor;
};

UCLASS()
class VOXTERRA_API AVoxTerraWorld : public AActor
{
	GENERATED_BODY()

public:
	AVoxTerraWorld();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "VoxTerra")
	USceneComponent* Root = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	UMaterialInterface* LandMaterialSolid = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	UMaterialInterface* LandMaterialTranslucent = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	UMaterialInterface* LandMaterialWater = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "VoxTerra")
	TMap<FIntVector, UTerrainChunkData*> Chunks;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	FVector PlayerPos = { 0, 0, 0 };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	double DistanceLoad = 1000;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	double DistanceUnload = 1300;

	TSharedPtr<FChunksManager> ChunksManager;

	UFUNCTION(CallInEditor, Category = "VoxTerra")
	void TestSectorGen();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	FIntVector TestSectorBaseChunkPos = { 0, 0, 0 };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	FVector2D TestSectorPos = { 0, 0 };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	UTexture2D* TestSectorTexture = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VoxTerra")
	UTexture2D* TestSectorTexture2 = nullptr;

	
};