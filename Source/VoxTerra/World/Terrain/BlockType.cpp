﻿#include "BlockType.h"

#include "BlockTypeInfo.h"

bool FBlockType::IsAir()
{
	return BlockType == BlockTypeAir.BlockType;
}
bool FBlockType::IsTranslucent()
{
	return BlockType >= TransparentBlocksStart;
	//return GBlocksTypeInfo[BlockType].bIsTranslucent;
}
bool FBlockType::IsSolid()
{
	return !IsAir() && !IsTranslucent();
}

bool FBlockType::IsNgen()
{
	return BlockType == BlockTypeNgen.BlockType;
}
bool FBlockType::IsWater()
{
	return BlockType == BlockTypeWater.BlockType;
}