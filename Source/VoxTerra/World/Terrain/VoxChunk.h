﻿#pragma once
#include "BlockType.h"
#include "VoxTerra/Common/Math.h"
#include "VoxChunk.generated.h"

enum class EAxis3d
{
	X = 0,
	Y = 1,
	Z = 2
};

enum class ESideBlock
{
	Xm = 0,
	Ym = 1,
	Zm = 2,
	Xp = 3,
	Yp = 4,
	Zp = 5
};

enum class ERefType
{
	Node,
	Leaf
};

inline constexpr uint32 LeafBitMask = 0x1 << 31;

USTRUCT()
struct FChunkNodeRef
{
	GENERATED_BODY()

	static FChunkNodeRef CreateNode(int Idx)
	{
		FChunkNodeRef Res{ Idx & ~LeafBitMask };
		return Res;
	}
	static FChunkNodeRef CreateLeaf(FBlockType Block)
	{
		return CreateLeaf(Block.BlockType);
	}
	static FChunkNodeRef CreateLeaf(int BlockId)
	{
		FChunkNodeRef Res{ (BlockId & ~LeafBitMask) | LeafBitMask };
		return Res;
	}

	uint32 Data = (uint32)-10;

	TPair<ERefType, int32> GetInfo() const
	{
		ERefType Type = (Data & LeafBitMask) == 0 ? ERefType::Node : ERefType::Leaf;
		int32 Idx = (Data & ~LeafBitMask);
		return { Type, Idx };
	}
};

USTRUCT()
struct FVoxChunkNode
{
	GENERATED_BODY()

	FChunkNodeRef Children[2][2][2];
};

USTRUCT()
struct FVoxChunkSideNode
{
	GENERATED_BODY()

	FChunkNodeRef Children[2][2];
};

struct FChunkMeshData
{
	TArray<FVector> Vertices;
	TArray<int> Triangles;
	TArray<FVector> Normals;
	TArray<FColor> Colors;
	TArray<FVector2d> UV;

	void Clear()
	{
		Vertices.Empty();
		Triangles.Empty();
		Normals.Empty();
		Colors.Empty();
		UV.Empty();
	}
};

enum class EBlockTreeType
{
	Empty,
	Full,
	Voxel
};

USTRUCT()
struct FVoxSide
{
	GENERATED_BODY()
	struct FLeafInfo
	{
		FIntVector2 Pos = { 0, 0 };
		FBlockType Block;
		int Level = 0;
	};

	TArray<FVoxChunkSideNode> Nodes;
	TOptional<FBlockType> Block;

	TArray<FLeafInfo> GetAllVoxels();

	FLeafInfo AtAdv(FIntVector2 const& PosSample);
};

// Chunk octotree voxels
// Empty Nodes Means Air chunk
USTRUCT()
struct FVoxChunk
{
	GENERATED_BODY()

	struct FLeafInfo
	{
		FIntVector Pos = FIntVector::ZeroValue;
		FBlockType Block;
		int Level = 0;
	};

	FVoxChunk();

	void Init(TFunction<FBlockType(FIntVector const& Pos)> const& GenFunc);
	void InitAdvanced(
		TFunction<FBlockType(FIntVector const& Pos)> const& GenFunc,
		TFunction<TOptional<FBlockType>(FIntVector const& Pos, int Level)> const& GenCubeInfo);
	void PackSidesData();
	void PackSideData(FVoxSide& SideInfo);
	void PackChunkData();

	FBlockType At(FIntVector const& Pos);
	FChunkNodeRef* AtMut(FIntVector const& Pos);
	FLeafInfo AtAdv(FIntVector const& PosSample);
	FLeafInfo AtAdvChunk(FIntVector const& PosSample);
	FLeafInfo AtAdvSide(ESideBlock Side, FIntVector const& PosSample);

	bool IsPosInChunk(FIntVector const& PosSample);
	// Return all leafs
	TArray<FLeafInfo> GetAllVoxels();
	void DebugRenderVoxels(int MaxDrawLevel);
	void DebugRenderSides();
	void DebugRenderVoxels2();
	void DebugRenderTreeLevel(TArray<FVoxChunkNode> const& NodesT);
	void GenMeshData();
	static int LevelCubeSize(int Level);
	static FVector GetSectorCenter(FIntVector const& SectorPos);

	// X   0
	// |\
	// X X 1
	// |\
	// X X 2
	// |\
	// X X 3
	// |\
	// X X 4
	// |\
	// X X 5
	// |\
	// X X 6
	// |\
	// X X 7 (TreeLevels-2)
	// |\
	// X X 8 (TreeLevels-1)
	// 9-256
	static constexpr int TreeLevels = 9;
	static constexpr int ChunkSize = 1 << (TreeLevels - 1);
	static constexpr double ChunkSphereSize = CSqrt((FVoxChunk::ChunkSize / 2.0) * (FVoxChunk::ChunkSize / 2.0) * 3);

	TArray<FVoxChunkNode> Nodes;
	TOptional<FBlockType> ChunkBlock;
	FVoxSide Sides[6];

	TSharedPtr<FChunkMeshData> MeshDataSolid;
	TSharedPtr<FChunkMeshData> MeshDataTranslucent;

	TSharedPtr<FChunkMeshData> MeshDataWater;
};