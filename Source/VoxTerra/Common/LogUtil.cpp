#include "LogUtil.h"

DEFINE_LOG_CATEGORY(LogVoxTerra);

void LOG_IMPLWarning(const char* File, int Line, const char* Function, FString const& Text)
{
	UE_LOG(LogVoxTerra, Warning, TEXT("%s (file:%s line:%i func:%s"), *Text, *FString(File), Line, *FString(Function));
}
void LOG_IMPLError(const char* File, int Line, const char* Function, FString const& Text)
{
	UE_LOG(LogVoxTerra, Error, TEXT("%s (file:%s line:%i func:%s"), *Text, *FString(File), Line, *FString(Function));
}
