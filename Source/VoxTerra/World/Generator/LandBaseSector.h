﻿#pragma once
#include "LandBaseGenerator.h"
#include "VoxTerra/Common/ArrayDim2.h"

class UProceduralMeshComponent;
struct FLandObjectInfo
{
	FIntVector Pos;
	int Type;
};

struct FLowResPixelInfo
{
	float MultHeightDensity = 0;
};

struct FLandBaseSector
{
	static constexpr int SectorSize = 1024;
	// static constexpr int TextureLowResMultiplier = 16;
	// static constexpr int TextureLowResSize = SectorSize / TextureLowResMultiplier;

	FIntVector2 SectorPos = { 0, 0 };

	TArrayDim2<FLandBasePixel> Texture;
	// TArrayDim2<FLowResPixelInfo> TextureLowRes;
	TArray<FLandObjectInfo> Objects;

	FLandBaseSector();

	void Generate(TSharedPtr<FLandBaseGenerator> const& Gen, FIntVector2 const& SectorPosNew);

	struct FDebugOutput
	{
		UTexture2D* Texture = nullptr;
		UTexture2D* Texture2 = nullptr;
		UProceduralMeshComponent* MeshComponent = nullptr;
	};

	void GetDebugInfo(FDebugOutput& Res);
};