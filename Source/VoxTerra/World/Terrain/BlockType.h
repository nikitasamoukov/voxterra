﻿#pragma once

struct FBlockType
{
	bool IsAir();
	bool IsTranslucent();
	bool IsSolid();
	bool IsNgen();
	bool IsWater();

	int32 BlockType = 0;

	friend bool operator==(const FBlockType& A, const FBlockType& B) = default;
};

// inline constexpr FBlockType BlockType = FBlockType{ 6 };