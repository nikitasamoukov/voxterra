﻿#pragma once

#include "VoxTerra/World/Biomes/BiomesInfo.h"

struct FLandBasePixel
{
	int16 Height = 0;
	EBiome Biome = EBiome::Unknown;
};

struct FLandBaseGenerator
{
	struct FImpl;

	explicit FLandBaseGenerator(int32 SeedNew);
	~FLandBaseGenerator();
	FLandBasePixel GetPixel(FIntVector2 const& Pos) const;

	int32 Seed = 0;

	TUniquePtr<FImpl> Impl;
};