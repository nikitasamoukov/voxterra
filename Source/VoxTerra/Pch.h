#pragma once
#include "CoreMinimal.h"
#include "Core.h"
#include "Engine.h"
#include "Common/LogUtil.h"

#define PROFILE_FUNCTION() TRACE_CPUPROFILER_EVENT_SCOPE_STR(__FUNCTION__)
#define PROFILE_SCOPE(Name) TRACE_CPUPROFILER_EVENT_SCOPE_STR(Name)