#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VoxTerraGameModeBase.generated.h"

UCLASS()
class VOXTERRA_API AVoxTerraGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};