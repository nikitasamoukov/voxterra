﻿#pragma once

enum class EBiome : uint8
{
	Unknown,
	DeepOcean,
	Ocean,
	ShallowWater,
	SandBeach,
	WaterSandBeach,
	GrassPlains,
	Forest,
	Mountains,
	HighMountains,
};

struct FBiomeInfo
{
	FColor Color = FColor::Black;
};

extern TMap<EBiome, FBiomeInfo> GBiomesInfo;