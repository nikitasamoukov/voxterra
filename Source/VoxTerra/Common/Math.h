﻿#pragma once

double constexpr CSqrtImpl(double x, double curr, double prev)
{
	return curr == prev
		? curr
		: CSqrtImpl(x, 0.5 * (curr + x / curr), curr);
}

double constexpr CSqrt(double x)
{
	return x >= 0 && x < std::numeric_limits<double>::infinity()
		?CSqrtImpl(x, x, 0)
		: std::numeric_limits<double>::quiet_NaN();
}