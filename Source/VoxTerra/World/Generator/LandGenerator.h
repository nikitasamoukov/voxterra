﻿#pragma once
#include "LandBaseCache.h"
#include "LandBaseGenerator.h"

struct FLandGenerator
{
	explicit FLandGenerator(int32 SeedNew)
	{
		LandBaseGen = MakeShared<FLandBaseGenerator>(SeedNew);
		LandBaseCache = MakeShared<FLandBaseCache>(LandBaseGen);
	}

	bool TryGetLandRect(FLandRectData& Data)
	{
		PROFILE_FUNCTION()
		return LandBaseCache->RequestPixels(Data);
	}

	TSharedPtr<FLandBaseCache> LandBaseCache;
	TSharedPtr<FLandBaseGenerator> LandBaseGen;
};