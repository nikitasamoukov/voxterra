﻿#pragma once

#include "TextureUtil.generated.h"

USTRUCT()
struct FImageCpuRgba8
{
	GENERATED_BODY()
	TArray<FColor> Data;
	int W = 0;
	int H = 0;

	void Init(int X, int Y)
	{
		Data.SetNum(0);
		W = X;
		H = Y;

		Data.InsertZeroed(0, W * H);
	}

	bool IsValid() const
	{
		return W > 0 && H > 0 && W * H == Data.Num();
	}

	FColor& At(int X, int Y)
	{
		return Data[X + Y * W];
	}

	void Upscale2()
	{
		FImageCpuRgba8 TextureUpscale2;
		TextureUpscale2.Init(W * 2, H * 2);

		for (int x = 0; x < W; x++)
			for (int y = 0; y < H; y++)
			{
				TextureUpscale2.At(x * 2 + 0, y * 2 + 0) = At(x, y);
				TextureUpscale2.At(x * 2 + 1, y * 2 + 0) = At(x, y);
				TextureUpscale2.At(x * 2 + 0, y * 2 + 1) = At(x, y);
				TextureUpscale2.At(x * 2 + 1, y * 2 + 1) = At(x, y);
			}
		*this = TextureUpscale2;
	}
};

UTexture2D* ConvCpuTextureToTexture2d(FImageCpuRgba8 const& TextureCpu);