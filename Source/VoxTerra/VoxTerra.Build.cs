// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class VoxTerra : ModuleRules
{
	public VoxTerra(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		PrivatePCHHeaderFile = "PCH.h";
		
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new [] {
			"ProceduralMeshComponent"
		});
		
		PrivateIncludePaths.Add("./../ThirdParty/include");
		CppStandard = CppStandardVersion.Cpp20;

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}