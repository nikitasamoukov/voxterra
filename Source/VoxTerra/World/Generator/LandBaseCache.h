﻿#pragma once
#include "LandBaseSector.h"

struct FLandRectData
{
	FIntVector2 PosBase;
	TArrayDim2<FLandBasePixel> Pixels;
};

struct FLandBaseCache : public TSharedFromThis<FLandBaseCache>
{
	explicit FLandBaseCache(TSharedPtr<FLandBaseGenerator> const& Gen);

	bool RequestPixels(FLandRectData& Data);
	FIntVector2 GetPixelSectorPos(FIntVector2 const& Pos);
	FIntVector2 GetPosInSector(FIntVector2 const& Pos);

	void EnqueueGen(FIntVector2 const& Pos);
	void OnSectorLoaded(FIntVector2 const& Pos);

	struct FCell
	{
		int64 Time = 0;
		TSharedPtr<FLandBaseSector> Sector;
		TFuture<TSharedPtr<FLandBaseSector>> LoadFuture;
	};

	static constexpr int CacheSectorsCount = 100; // ~500 Mb

private:
	FCriticalSection Mutex;
	TSharedPtr<FLandBaseGenerator> LandBaseGen;
	TMap<FIntVector2, FCell> Sectors;
};