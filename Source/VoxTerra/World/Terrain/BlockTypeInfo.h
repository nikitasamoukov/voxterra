﻿#pragma once
#include "BlockType.h"

struct FBlockTypeInfo
{
	FColor Color = FColor::Purple;
	bool bIsTranslucent = false;
};

inline constexpr FBlockType BlockTypeAir = FBlockType{ 0 };
inline constexpr FBlockType BlockTypeNgen = FBlockType{ 1 };

inline constexpr FBlockType BlockTypeGrass = FBlockType{ 2 };
inline constexpr FBlockType BlockTypeDirt = FBlockType{ 3 };
inline constexpr FBlockType BlockTypeStone = FBlockType{ 4 };
inline constexpr FBlockType BlockTypeSand = FBlockType{ 5 };
inline constexpr FBlockType BlockTypeSnow = FBlockType{ 6 };

inline constexpr FBlockType BlockTypeStoneBlock = FBlockType{ 8 };
inline constexpr FBlockType BlockTypeWoodPlanks = FBlockType{ 9 };
inline constexpr FBlockType BlockTypeConcrete = FBlockType{ 10 };

inline constexpr int TransparentBlocksStart = 100;

inline constexpr FBlockType BlockTypeGlass = FBlockType{ TransparentBlocksStart + 0 };

inline constexpr FBlockType BlockTypeWater = FBlockType{ 150 };

extern const TArray<FBlockTypeInfo> GBlocksTypeInfo;