﻿#include "LandBaseCache.h"

FLandBaseCache::FLandBaseCache(TSharedPtr<FLandBaseGenerator> const& Gen)
{
	LandBaseGen = Gen;
}

bool FLandBaseCache::RequestPixels(FLandRectData& Data)
{
	PROFILE_FUNCTION()
	FScopeLock Lock(&Mutex);

	auto& Pixels = Data.Pixels;

	FIntVector2 MinSectorPos = GetPixelSectorPos(Data.PosBase);
	FIntVector2 MaxSectorPos = GetPixelSectorPos({ Data.PosBase.X + Data.Pixels.Num().X - 1, Data.PosBase.Y + Data.Pixels.Num().Y - 1 });

	bool bRequiredSectorsExist = true;
	for (int x = MinSectorPos.X; x <= MaxSectorPos.X; x++)
		for (int y = MinSectorPos.Y; y <= MaxSectorPos.Y; y++)
		{
			FIntVector2 SectorPos(x, y);
			if (!Sectors.Contains(SectorPos))
			{
				EnqueueGen(SectorPos);
				bRequiredSectorsExist = false;
			}
			else
			{
				if (!Sectors[SectorPos].Sector)
					bRequiredSectorsExist = false;
			}
		}

	if (!bRequiredSectorsExist)
	{
		return false;
	}

	for (int x = 0; x < Pixels.Num().X; x++)
		for (int y = 0; y < Pixels.Num().Y; y++)
		{
			FIntVector2 PosPixel(x + Data.PosBase.X, y + Data.PosBase.Y);

			auto SectorPos = GetPixelSectorPos(PosPixel);
			if (!Sectors.Contains(SectorPos))
			{
				EnqueueGen(SectorPos);
			}

			auto PosInSector = GetPosInSector(PosPixel);
			Pixels.At(x, y) = Sectors[SectorPos].Sector->Texture.At(PosInSector);
		}

	for (int o = 0; o < 10; o++)
		if (Sectors.Num() >= CacheSectorsCount)
		{
			TOptional<FIntVector2> PosLow;
			for (auto& Pair : Sectors)
			{
				if (!PosLow)
				{
					PosLow = Pair.Key;
					continue;
				}
				if (Pair.Value.Time < Sectors[*PosLow].Time)
					PosLow = Pair.Key;
			}

			Sectors.Remove(*PosLow);
		}

	return true;
}

int ModSs(int A)
{
	return ((A % FLandBaseSector::SectorSize) + FLandBaseSector::SectorSize) % FLandBaseSector::SectorSize;
}

int DivSs(int A)
{
	return (A - ModSs(A)) / FLandBaseSector::SectorSize;
}

FIntVector2 FLandBaseCache::GetPixelSectorPos(FIntVector2 const& Pos)
{
	return { DivSs(Pos.X), DivSs(Pos.Y) };
}

FIntVector2 FLandBaseCache::GetPosInSector(FIntVector2 const& Pos)
{
	return { ModSs(Pos.X), ModSs(Pos.Y) };
}

void FLandBaseCache::EnqueueGen(FIntVector2 const& Pos)
{
	if (Sectors.Contains(Pos))
		return;

	TFunction<TSharedPtr<FLandBaseSector>()> Task = [LandBaseGenL = LandBaseGen, SectorPos = Pos]() -> TSharedPtr<FLandBaseSector> {
		PROFILE_SCOPE("Task generate land rect")

		auto NewSector = MakeShared<FLandBaseSector>();
		NewSector->Generate(LandBaseGenL, SectorPos);

		return NewSector;
	};
	TFunction<void()> OnTaskFinish = [LandBaseCache = this->AsShared(), SectorPos = Pos]() {
		LandBaseCache->OnSectorLoaded(SectorPos);
	};

	Sectors.Add(Pos, {});

	Sectors[Pos].LoadFuture = Async(EAsyncExecution::ThreadPool, Task, OnTaskFinish);
}

void FLandBaseCache::OnSectorLoaded(FIntVector2 const& Pos)
{
	FScopeLock Lock(&Mutex);

	auto Res = Sectors[Pos].LoadFuture.Get();

	Sectors[Pos].LoadFuture.Reset();
	Sectors[Pos].Sector = Res;
}